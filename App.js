import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { createSwitchNavigator, createStackNavigator,createBottomTabNavigator,createDrawerNavigator } from 'react-navigation';

import splash from './src/splashscreen'
import splash2 from './src/splash2'
import intro from './src/auth/intro'
import menu from './src/menu'

import berita from './src/fitur/berita'
import detailberita from './src/fitur/detailberita'

import login from './src/auth/login'
import register from './src/auth/register'

import pengaduan from './src/fitur/pengaduan/pengaduan'
import form from './src/fitur/pengaduan/form'

import layanan from './src/fitur/layanan/layanan'
import polisi from './src/fitur/layanan/polisi'
import pemadam from './src/fitur/layanan/pemadam'
import kesehatan from './src/fitur/layanan/kesehatan'
import detaillayanan from './src/fitur/layanan/detail'

import ibadah from './src/fitur/ibadah/ibadah'
import jenisibadah from './src/fitur/ibadah/jenis'
import detailibadah from './src/fitur/ibadah/detail'

import kkesehatan from './src/fitur/kesehatan/kesehatan'
import jeniskesehatan from './src/fitur/kesehatan/jenis'
import detailkesehatan from './src/fitur/kesehatan/detail'

import pendidikan from './src/fitur/pendidikan/pendidikan'
import jenispendidikan from './src/fitur/pendidikan/jenis'
import detailpendidikan from './src/fitur/pendidikan/detail'
import webpendidikan from './src/fitur/pendidikan/webpendidikan'

import pekerjaan from './src/fitur/pekerjaan/pekerjaan'
import detailpekerjaan from './src/fitur/pekerjaan/detail'
import arsip from './src/fitur/pekerjaan/arsip'
import jenispekerjaan from './src/fitur/pekerjaan/jenis'

import perizinan from './src/fitur/perizinan/perizinan'
import subperizinan from './src/fitur/perizinan/subperizinan'
import detailperizinan from './src/fitur/perizinan/detail'
import web from './src/fitur/perizinan/web'

import pariwisata from './src/fitur/pariwisata/pariwisata'
import infopriwisata from './src/fitur/pariwisata/info/info'
import detailinfo from './src/fitur/pariwisata/info/detail'

import event from './src/fitur/pariwisata/event/event'
import konser from './src/fitur/pariwisata/event/konser'
import festival from './src/fitur/pariwisata/event/festival'
import bazaar from './src/fitur/pariwisata/event/bazaar'
import detailevent from './src/fitur/pariwisata/event/detail'

import kuliner from './src/fitur/pariwisata/kuliner/kuliner'
import detailkuliner from './src/fitur/pariwisata/kuliner/detail'

import hotel from './src/fitur/pariwisata/hotel/hotel'
import detailhotel from './src/fitur/pariwisata/hotel/detailhotel'
// import filterhotel from './src/fitur/pariwisata/hotel/filterhotel'

import SideMenu from './src/sideMenu'

import transportasi from './src/fitur/transportasi/transportasi'

import bus from './src/fitur/transportasi/bus'
import detailbus from './src/fitur/transportasi/detailbus'

import kereta from './src/fitur/transportasi/kereta'
import detailkereta from './src/fitur/transportasi/detailkereta'

import rental from './src/fitur/transportasi/rental'
import datailrental from './src/fitur/transportasi/datailrental'

import kendaraan from './src/fitur/transportasi/kendaraan'
import detailkendaraan from './src/fitur/transportasi/detailkendaraan'

import destinasi from './src/fitur/pariwisata/destinasi/destinasi'
import detaildestinasi from './src/fitur/pariwisata/destinasi/detaildestinasi'

import kuning from './src/fitur/kuning/kuning'
import kuningsub from './src/fitur/kuning/sub'

const Auth = createStackNavigator({
  Intro     : intro, 
  Register  : register,
  Login     : login,
},{headerMode   : 'none'});

const layanandt = createStackNavigator({
  Layanan           : layanan,
  polisi            : polisi,
  pemadam           : pemadam,
  kesehatan         : kesehatan,
  DetailLayanan     : detaillayanan,
},{headerMode   : 'none'})

const pengaduandt = createStackNavigator({
  Pengaduan         : pengaduan,
  Form              : form,
},{headerMode   : 'none'})

const perizinandt = createStackNavigator({
  Perizinan         : perizinan,
  Subperizinan      : subperizinan,
  Detailperizinan   : detailperizinan,
  Web               : web,
},{headerMode   : 'none'})

const kesehatandt = createStackNavigator({
  Jeniskesehatan    : jeniskesehatan,
  kkesehatan        : kkesehatan,
  DetailKesehatan   : detailkesehatan,
},{headerMode   : 'none'})

const pendidikandt = createStackNavigator({
  Jenispendidikan   : jenispendidikan,
  Pendidikan        : pendidikan,
  DetailPendidikan  : detailpendidikan,
  WebPendidikan     : webpendidikan,
},{headerMode   : 'none'})

const ibadahdt = createStackNavigator({
  Jenisibadah    : jenisibadah,
  Ibadah         : ibadah,
  DetailIbadah   : detailibadah,
},{headerMode   : 'none'})

const pekerjaandt = createStackNavigator({
  JenisPekerjaan : jenispekerjaan,
  Kuning         : kuning,
  Sub            : kuningsub,
  Pekerjaan      : pekerjaan,
  DetailPekerjaan: detailpekerjaan,
  Arsip          : arsip,
},{headerMode   : 'none'})

const eventdt = createBottomTabNavigator({
  All                : event,
  Konser             : konser,
  Festival           : festival,
  Bazaar             : bazaar,
},{
    navigationOptions:{ header:{ visible:false }},
    lazy:false,
    tabBarPosition: 'bottom',
    animationEnabled: true,
    swipeEnabled: true,
    tabBarTextFontFamily: 'quicksand',
    tabBarOptions: {
      activeTintColor: '#209CEE',
      showIcon: true,
      showLabel: true,
    },
})

const pariwisatadt = createStackNavigator({
  Pariwisata        : pariwisata,
  //
  Hotel             : hotel,
  DetailHotel       : detailhotel,
  // FilterHotel       : filterhotel,
  //
  Kuliner           : kuliner,
  DetailKuliner     : detailkuliner,
  //
  Info              : infopriwisata,
  DetailInfo        : detailinfo,
  //
  Event             : eventdt,
  DetailEvent       : detailevent,
  //
  Destinasi         : destinasi,
  DetailDestinasi   : detaildestinasi,
},{headerMode   : 'none'})


const transportasidt = createStackNavigator({
  Transportasi        : transportasi,
  Kendaraan           : kendaraan,
  DetailKendaraan     : detailkendaraan,
  Rental              : rental,
  DatailRental        : datailrental,
  Kereta              : kereta,
  DetailKereta        : detailkereta,
  Bus                 : bus,
  DetailBus           : detailbus,
},{headerMode   : 'none'})

const beritadt = createStackNavigator({
  Berita        : berita,
  DetailBerita  : detailberita,
},{headerMode   : 'none'})

const Main = createDrawerNavigator({
  /////
  Menu           : menu,
  pariwisatadt   : pariwisatadt,
  pendidikandt   : pendidikandt,
  Ibadahdt       : ibadahdt,
  transportasidt : transportasidt,
  pengaduandt    : pengaduandt,
  perizinandt    : perizinandt,
  layanandt      : layanandt,
  beritadt       : beritadt,
  Pekerjaandt    : pekerjaandt,
  kesehatandt    : kesehatandt,
  ////
},{
    contentComponent: SideMenu,
    drawerWidth: 280,
    drawerLockMode: 'locked-open',
    activeTintColor: '#209CEE'
})

const Splashscreen = createStackNavigator({ 
  Splash : splash
},{headerMode   : 'none'});

export default createSwitchNavigator(
  {
    Splashscreen  : Splashscreen,
    Auth          : Auth,
    Main          : Main
  },
  {
    initialRouteName: 'Splashscreen',
  }
);