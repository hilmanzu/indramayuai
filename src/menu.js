import React, {Component} from 'react';
import {Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from './style/style'
import store from 'react-native-simple-store';
import {PermissionsAndroid} from 'react-native';
import Sholat from './style/sholat'
import Carousel from 'react-native-snap-carousel';

const { width: viewport } = Dimensions.get('window')
const sliderWidth1 = wp(100)
const itemHorizontalMargin1 = wp(0.36)
const sliderWidth = viewport
const itemWidth = (sliderWidth1 + (itemHorizontalMargin1 * 2)) - 15

function wp(percentage){
  const value = (percentage * viewport) / 100
  return Math.round(value)
}

export default class menu extends Component{

  constructor(props) {
    super(props)
      this.state = {
        dataSource: '',
        loading: true,
        jadwal:'',
        data:[],
      }
  }

  componentDidMount(){
      const { navigate } = this.props.navigation;
      fetch( 'https://indramayuapp.nusantech.co/v1/banner')
      .then((response) => response.json())
      .then((responseJson) =>{
          this.setState({
            data: responseJson.data , loading:false
          })
      })
        .catch((error) =>{
          console.error(error);
      })

  }

   _renderItem ({item, index}) {
        return (
            <View style={{height:150,width:320,marginTop:10}}>
                <Image source={{uri: 'https://indramayuapp.nusantech.co/uploads/banner/' + item.image}} style={{position:'absolute',width:Dimensions.get('window').width,height:150}}/>
            </View>
        );
    }


  render() {
    const { navigate } = this.props.navigation;
    const { data } = this.state

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity onPress={() => {this.props.navigation.openDrawer()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('./image/menu.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>Indramayu All In One</Text>
        </View>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center'}}>
            <Image source={require('./image/back2.png')} style={styles.ImageLogo2}/>
            <Carousel
              ref={(c) => { this._carousel = c; }}
              data={data}
              autoplay = {true}
              loop = {true}
              renderItem={this._renderItem}
              sliderWidth={sliderWidth}
              itemWidth={itemWidth}
            />
            <TouchableOpacity style={styles.layananbuttonimage} onPress={() => this.props.navigation.navigate('Layanan')}>
              <View style={styles.layananbutton}>
                <Text multiline={false} style={styles.layananbuttontext}>Layanan Darurat</Text>
                <Image source={require('./image/layanan.png')} style={styles.imagelayanan}/>
              </View>
            </TouchableOpacity>
            <View style={{flexDirection:'row',marginTop:3}}>
              <TouchableOpacity style={styles.layanancategory} onPress={() => this.props.navigation.navigate('Pengaduan')}>
                <Image source={require('./image/Pengaduan.png')} style={styles.imagecategory}/>
                <Text style={styles.textcategory}>Pengaduan</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.layanancategory} onPress={() => this.props.navigation.navigate('Jeniskesehatan')}>
                <Image source={require('./image/Hospital.png')} style={styles.imagecategory}/>
                <Text style={styles.textcategory}>Kesehatan</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.layanancategory} onPress={() => this.props.navigation.navigate('Jenisibadah')}>
                <Image source={require('./image/peribadatan.png')} style={styles.imagecategory}/>
                <Text style={styles.textcategory}>Ibadah</Text>
              </TouchableOpacity>
            </View>
            <View style={{flexDirection:'row',marginTop:3}}>
              <TouchableOpacity style={styles.layanancategory} onPress={() => this.props.navigation.navigate('JenisPekerjaan')}>
                <Image source={require('./image/Loker.png')} style={styles.imagecategory}/>
                <Text style={styles.textcategory}>Lowongan</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.layanancategory} onPress={() => this.props.navigation.navigate('Pariwisata')}>
                <Image source={require('./image/pariwisata.png')} style={styles.imagecategory}/>
                <Text style={styles.textcategory}>Pariwisata</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.layanancategory} onPress={() => this.props.navigation.navigate('Jenispendidikan')}>
                <Image source={require('./image/education.png')} style={styles.imagecategory}/>
                <Text style={styles.textcategory}>Pendidikan</Text>
              </TouchableOpacity>
            </View>
            <View style={{flexDirection:'row',marginTop:3}}>
              <TouchableOpacity style={styles.layanancategory} onPress={() => this.props.navigation.navigate('Perizinan')}>
                <Image source={require('./image/Perizinan.png')} style={styles.imagecategory}/>
                <Text style={styles.textcategory}>Perizinan</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.layanancategory} onPress={() => this.props.navigation.navigate('Transportasi')}>
                <Image source={require('./image/transportasi.png')} style={styles.imagecategory}/>
                <Text style={styles.textcategory}>Kendaraan</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.layanancategory} onPress={() => this.props.navigation.navigate('Berita')}>
                <Image source={require('./image/news.png')} style={styles.imagecategory}/>
                <Text style={styles.textcategory}>Berita</Text>
              </TouchableOpacity>
            </View>
            <View style={{marginBottom:50,alignItems:'center'}}>
              <Sholat/>
            </View>
          </ScrollView>
      </View>
    );
  }
  keluar=()=>{
    this.props.navigation.navigate('Splash')
    store.delete('token')
  }
}
