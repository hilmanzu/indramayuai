import React, {Component} from 'react';
import {ActivityIndicator,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import store from 'react-native-simple-store';
import {PermissionsAndroid} from 'react-native';

export default class menu extends Component{

  constructor(props) {
    super(props)
      this.state = {
        dataSource: '',
        loading: true,
        jadwal:''
      }
  }

  componentDidMount(){
      store.get('position').then((res) =>{
          if (res.initialPosition.latitude) {
            fetch('https://time.siswadi.com/pray/' + res.initialPosition.latitude+ '/' + res.initialPosition.longitude, {
              method: 'GET',
              headers: {  
                    'Accept':'application/json',
                    'Content-Type':'application/json'}
                  })
              .then((response) => response.json())
              .then((responseJson) =>{
                console.log(responseJson)
                this.setState({
                  jadwal : responseJson,loading:false
                })
              })
          }else{
            fetch('https://time.siswadi.com/pray/-6.327583/108.324936', {
              method: 'GET',
              headers: {  
                    'Accept':'application/json',
                    'Content-Type':'application/json'}
                  })
              .then((response) => response.json())
              .then((responseJson) =>{
                console.log(responseJson)
                this.setState({
                  jadwal : responseJson,loading:false
                })
              })
          }
      })
  }


  render() {
    const {jadwal,loading} = this.state

    if (loading === true) {
        return(
          <View style={{padding:10,borderRadius:10,alignItem:'center',justifyContent:'center',marginTop:20,width:340,height:150,backgroundColor:'#fff',elevation:10}}>
            <Text style={{textAlign:'center',color:'#007CBF'}}>Menentukan Jadwal Sholat</Text>
            <Text style={{textAlign:'center',color:'#007CBF'}}>Harap Menunggu</Text>
            <ActivityIndicator
              color={`rgba(0, 163, 192, 1)`}
              size={`small`}
              style={{marginTop:5}}
            />
          </View>
        )
      }

    return (
        <View style={{padding:5,borderRadius:10,flexDirection:'column',marginTop:20,width:340,height:150,backgroundColor:'#fff',elevation:10}}>
          <Text style={{textAlign:'center',color:'#007CBF',marginVertical:10,fontWeight:'bold'}}>-- Jadwal Sholat --</Text>
          <View style={{flexDirection:'row',flex:1}}>
            <View style={{flex:1}}>
              <Text style={{flex:1,textAlign:'center',color:'#007CBF',fontWeight:'bold'}}>Subuh</Text>
              <Text style={{flex:1,textAlign:'center',color:'#000000'}}>{jadwal.data.Fajr}</Text>
            </View>
            <View style={{flex:1}}>
              <Text style={{flex:1,textAlign:'center',color:'#007CBF',fontWeight:'bold'}}>Dzuhur</Text>
              <Text style={{flex:1,textAlign:'center',color:'#000000'}}>{jadwal.data.Dhuhr}</Text>
            </View>
            <View style={{flex:1}}>
              <Text style={{flex:1,textAlign:'center',color:'#007CBF',fontWeight:'bold'}}>Ashar</Text>
              <Text style={{flex:1,textAlign:'center',color:'#000000'}}>{jadwal.data.Asr}</Text>
            </View>
          </View>
          <View style={{flexDirection:'row',flex:1}}>
            <View style={{flex:1}}>
              <Text style={{flex:1,textAlign:'center',color:'#007CBF',fontWeight:'bold'}}>Maghrib</Text>
              <Text style={{flex:1,textAlign:'center',color:'#000000'}}>{jadwal.data.Maghrib}</Text>
            </View>
            <View style={{flex:1}}>
              <Text style={{flex:1,textAlign:'center',color:'#007CBF',fontWeight:'bold'}}>Isa</Text>
              <Text style={{flex:1,textAlign:'center',color:'#000000'}}>{jadwal.data.Isha}</Text>
            </View>
            <View style={{flex:1}}>
              <Text style={{flex:1,textAlign:'center',color:'#007CBF',fontWeight:'bold'}}>Tahajjud</Text>
              <Text style={{flex:1,textAlign:'center',color:'#000000'}}>{jadwal.data.TengahMalam}</Text>
            </View>
          </View>
        </View>
    );
  }
}
