import React, { Component } from "react";
import { PermissionsAndroid,Text,Image, StyleSheet, ActivityIndicator, View } from "react-native";
import store from 'react-native-simple-store';
import DropdownAlert from 'react-native-dropdownalert';

export default class splashscreen extends Component {

  watchID: ?number = null
  componentDidMount(){
  const { navigate } = this.props.navigation;

    store.get('token').then((uid) =>{
      this.watchID = navigator.geolocation.watchPosition((position)=>{
        var lat = parseFloat(position.coords.latitude)
        var long = parseFloat(position.coords.longitude)
        var lastRegion = {
          latitude: lat,
          longitude: long,
          longitudeDelta:0.01,
          latitudeDelta: 0.01,
        }
        store.save('position',{initialPosition: lastRegion})
      },{enableHighAccuracy:true,timeout:2000,maximumAge:1000})

      store.get('position').then((res) =>{
        if(uid && res.initialPosition.latitude){setTimeout(() => this.props.navigation.navigate('Menu'), 2000)}
        else if (res.initialPosition.latitude){setTimeout(() => this.props.navigation.navigate('Intro'), 2000)}
      }),this.timer = setTimeout(() => this.componentDidMount(), 2000)

    })
  } 

  render(){
    return (
      <View style={styles.container}>
        <Image source={require('./image/logo.png')} style={styles.image} />
          <ActivityIndicator
            color={`rgba(0, 163, 192, 1)`}
            size={`large`}
            style={styles.spin}
          />
        <Text style={{marginTop:10}}>Sedang Menentukan Lokasi</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  image: {
    width: 200,
    height: 200,
    marginBottom:20
  }
});