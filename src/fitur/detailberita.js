import React, {Component} from 'react';
import {Dimensions,Picker,ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from './style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import Communications from 'react-native-communications';
import store from 'react-native-simple-store';
const haversine = require('haversine')
import getDirections from 'react-native-google-maps-directions'
import HTML from 'react-native-render-html';
import moment from 'moment'

const image = 'https://3.bp.blogspot.com/-_Ll9XLApuAA/V9qPPtjS5AI/AAAAAAAAAPk/5qnNrYXaLm8-iACn0l1dYpKGwnqX6LhlACLcB/s1600/Terminal%2BBus%2BIndramayu.jpg'

export default class detail extends Component{

   handleGetDirections(e){
    const data = {
      source: {
        latitude: e.deslatitude,
        longitude: e.deslongitude
      },
      destination: {
        latitude: e.latitude,
        longitude: e.longitude
      },
      params: [
        {
          key: "travelmode",
          value: "driving"        // may be "walking", "bicycling" or "transit" as well
        },
        {
          key: "dir_action",
          value: "navigate"       // this instantly initializes navigation using the given travel mode 
        }
      ]
    }
    getDirections(data)
  }

  render() {
    const {params} = this.props.navigation.state;

    return (
      <View style={styles.subcontainer}>
        <View style={styles.header}>
        <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>Detail Berita</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom:10}}>
          <Image source={{uri : params.gambar}} style={{width:Dimensions.get('window').width,height:170}}/>
          <Text style={{marginLeft:20,width:300,fontWeight:'bold',fontSize:18,marginTop:10,color:'#000000'}}>
              {params.judul}
          </Text>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontSize:14,color:'#007CBF'}}>
              {moment(params.waktu).format('DD MMMM YYYY')} WIB
          </Text>
          <View style={{marginHorizontal:20}}>
            <HTML html={params.deskripsi} imagesMaxWidth={Dimensions.get('window').width} />
          </View>
        </ScrollView>
      </View>
    );
  }
}