import React, {Component} from 'react';
import {ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert,Picker} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import Communications from 'react-native-communications';
import store from 'react-native-simple-store';
const haversine = require('haversine')
import getDirections from 'react-native-google-maps-directions'
import renderIf from './renderIf'

export default class kereta extends Component{
  constructor(props){
    super(props);
    this.state = {
      showAlert: false,
      loading:true,
      data :[],
      datas:[],
      category:''
      }
    }

  componentWillMount(){
      const { params } = this.props.navigation.state;
      store.get('position').then((res) =>{
        fetch('https://indramayuapp.nusantech.co/v1/transportasibus', {
          method: 'GET',
          headers: {  
                'Accept':'application/json',
                'Content-Type':'application/json'}
              })
        .then((response) => response.json())
        .then((responseJson) =>{
          data = responseJson.data
            this.setState({
              data : data,loading:false
            })
        })
        fetch('https://indramayuapp.nusantech.co/v1/masterrutebus', {
          method: 'GET',
          headers: {  
                'Accept':'application/json',
                'Content-Type':'application/json'}
              })
        .then((response) => response.json())
        .then((responseJson) =>{
          data = responseJson.data
            this.setState({
              datas : data,loading:false
            })
        })
      })
  }

  render() {
    const { data,loading,datas,category } = this.state
    const { params } = this.props.navigation.state;
    var re = /(<[^>]+>|<[^>]>|<\/[^>]>)/g;
    var filter = data.filter((item) => item.RuteBuses[0].MasterRuteBus.ruteName === this.state.category)

    if (loading === true) {
        return(
          <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
            <ActivityIndicator
              color={`rgba(0, 163, 192, 1)`}
              size={`large`}
              style={styles.s34e24b9b}
            />
            <Text style={{paddingTop:20}}> Menunggu Sebentar </Text>
          </View>
        )
      }

    return (
      <View style={styles.subcontainer}>
        <Image style={styles.ImageLogo} source={require('../image/back2.png')}/>
        <View style={styles.header}>
        <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>Bus Transportasi</Text>
        </View>
        <View style={styles.const}>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center',marginTop:20}}>
          <View style={{backgroundColor:'#fff',marginTop:20,width:320,height:120,alignItems:'center',elevation:5,borderRadius:5,marginBottom:20,borderWidth: 0.5,borderColor: '#149BE5'}}>
            <Text style={{fontWeight:'bold',width:100,marginTop:10,marginRight:170,marginBottom:15}}>
              Jurusan
            </Text>
            <View style={styles.itempekerjaan}>
              <Picker
                selectedValue={this.state.category}
                onValueChange={(category) => this.setState({category})}>
                <Picker.Item label='Pilih Jurusan' value={''} />
                {datas.map((item, index) => {
                return (<Picker.Item label={item.ruteName} value={item.ruteName} key={item.id}/>) 
                })}
              </Picker>
            </View>
          </View>
          <View style={{marginBottom:30}}>
            {data.map((data)=>
                    <View style={{marginHorizontal:2}}>
                    {renderIf(category !== '' ? false : true)(
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailBus',{rute: data.RuteBuses[0].MasterRuteBus.ruteName,nama:data.busName,waktu:data.RuteBuses[0].MasterRuteBus.RuteTerminals[0].waktuBerangkat,kapasitas:data.busKapasitas,terminal:data.RuteBuses[0].MasterRuteBus.RuteTerminals[0].Terminal.terminalName,utama:data.RuteBuses[0].MasterRuteBus.RuteTerminals[0].Terminal.Kotum.kotaName})} style={{width:320,height:70,backgroundColor:'#fff',borderRadius:5,elevation:5,justifyContent:'center',borderWidth: 0.5,borderColor: '#149BE5'}}>
                        <Text numberOfLines={2} style={{width:250,fontSize:12,color:'#149BE5',marginLeft:15}}>{data.RuteBuses[0].MasterRuteBus.ruteName}</Text>
                        <Text numberOfLines={2} style={{width:250,fontSize:12,color:'#149BE5',marginLeft:15,fontWeight:'bold'}}>{data.busName}</Text>
                      </TouchableOpacity>
                    )}
                    </View>
              )
            }
            {filter.map((data)=>
                    <View style={{marginHorizontal:2}}>
                    {renderIf(category == '' ? false : true)(
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailBus',{rute: data.RuteBuses[0].MasterRuteBus.ruteName,nama:data.busName,waktu:data.RuteBuses[0].MasterRuteBus.RuteTerminals[0].waktuBerangkat,kapasitas:data.busKapasitas,terminal:data.RuteBuses[0].MasterRuteBus.RuteTerminals[0].Terminal.terminalName,utama:data.RuteBuses[0].MasterRuteBus.RuteTerminals[0].Terminal.Kotum.kotaName})} style={{width:320,height:70,backgroundColor:'#fff',borderRadius:5,elevation:5,justifyContent:'center',borderWidth: 0.5,borderColor: '#149BE5'}}>
                        <Text numberOfLines={2} style={{width:250,fontSize:12,color:'#149BE5',marginLeft:15}}>{data.RuteBuses[0].MasterRuteBus.ruteName}</Text>
                        <Text numberOfLines={2} style={{width:250,fontSize:12,color:'#149BE5',marginLeft:15,fontWeight:'bold'}}>{data.busName}</Text>
                      </TouchableOpacity>
                    )}
                    </View>
              )
            }
          </View>
        </ScrollView>
        </View>
      </View>
    );
  }
}