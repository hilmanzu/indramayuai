import React, {Component} from 'react';
import {Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../style/style'
import store from 'react-native-simple-store';
import {PermissionsAndroid} from 'react-native';
import Carousel from 'react-native-snap-carousel';

const { width: viewport } = Dimensions.get('window')
const sliderWidth1 = wp(100)
const itemHorizontalMargin1 = wp(0.36)
const sliderWidth = viewport
const itemWidth = (sliderWidth1 + (itemHorizontalMargin1 * 2)) - 15

function wp(percentage){
  const value = (percentage * viewport) / 100
  return Math.round(value)
}

export default class transportasi extends Component{

  constructor(props) {
    super(props)
      this.state = {
        dataSource: '',
        loading: true,
        jadwal:'',
        data:[]
      }
  }

  componentDidMount(){
    fetch( 'https://indramayuapp.nusantech.co/v1/banner')
      .then((response) => response.json())
      .then((responseJson) =>{
          this.setState({
            data: responseJson.data , loading:false
          })
      })
        .catch((error) =>{
          console.error(error);
      })
  }

   _renderItem ({item, index}) {
        return (
            <View style={{height:150,width:320,marginTop:10}}>
                <Image source={{uri: 'https://indramayuapp.nusantech.co/uploads/banner/' + item.image}} style={{position:'absolute',width:Dimensions.get('window').width,height:150}}/>
            </View>
        );
    }

  render() {
    const { navigate } = this.props.navigation;
    const { data } = this.state
    
    return (
      <View style={{flex: 1,alignItems:'center',backgroundColor:'#fff'}}>
        <View style={styles.header}>
          <View style={{position:'absolute',zIndex: 1}}>
            <TouchableOpacity onPress={() => {this.props.navigation.openDrawer()}}>
              <Image source={require('../image/menu.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
            </TouchableOpacity>
          </View>
          <Text style={styles.headertext}>Transportasi</Text>
        </View>
        <View style={{alignItems:'center'}}>
            <Image source={require('../image/back2.png')} style={styles.ImageLogo2}/>
             <Carousel
              ref={(c) => { this._carousel = c; }}
              data={data}
              autoplay = {true}
              loop = {true}
              renderItem={this._renderItem}
              sliderWidth={sliderWidth}
              itemWidth={itemWidth}
            />
          <View style={{position:'absolute',marginTop:170}}>
            <View style={{marginTop:3}}>
              <TouchableOpacity style={{flexDirection:'row',marginTop:10}} onPress={() => this.props.navigation.navigate('Bus')}>
                <View style={{position:'absolute',marginLeft:80,marginTop:18,borderWidth: 0.5,borderColor: '#149BE5',backgroundColor:'#FFFFFF',width:240,height:55,borderRadius:10,justifyContent:'center'}}>
                  <Text style={{fontSize:15,marginLeft:20,color:'#149BE5',fontWeight:'bold'}}>Bus</Text>
                </View>
                <Image style={{width:90,height:73}} source={require('./image/bus.png')}/>
              </TouchableOpacity>
              <TouchableOpacity style={{flexDirection:'row',marginTop:10}} onPress={() => this.props.navigation.navigate('Kereta')}>
                <View style={{position:'absolute',marginLeft:80,marginTop:18,borderWidth: 0.5,borderColor: '#149BE5',backgroundColor:'#FFFFFF',width:240,height:55,borderRadius:10,justifyContent:'center'}}>
                  <Text style={{fontSize:15,marginLeft:20,color:'#149BE5',fontWeight:'bold'}}>Kereta</Text>
                </View>
                <Image style={{width:90,height:73}} source={require('./image/kereta.png')}/>
              </TouchableOpacity>
              <TouchableOpacity style={{flexDirection:'row',marginTop:10}} onPress={() => this.props.navigation.navigate('Rental')}>
                <View style={{position:'absolute',marginLeft:80,marginTop:18,borderWidth: 0.5,borderColor: '#149BE5',backgroundColor:'#FFFFFF',width:240,height:55,borderRadius:10,justifyContent:'center'}}>
                  <Text style={{fontSize:15,marginLeft:20,color:'#149BE5',fontWeight:'bold'}}>Rental</Text>
                </View>
                <Image style={{width:90,height:73}} source={require('./image/rental.png')}/>
              </TouchableOpacity>
              <TouchableOpacity style={{flexDirection:'row',marginTop:10}} onPress={() => this.props.navigation.navigate('Kendaraan')}>
                <View style={{position:'absolute',marginLeft:80,marginTop:18,borderWidth: 0.5,borderColor: '#149BE5',backgroundColor:'#FFFFFF',width:240,height:55,borderRadius:10,justifyContent:'center'}}>
                  <Text style={{fontSize:15,marginLeft:20,color:'#149BE5',fontWeight:'bold'}}>Kendaraan Dalam Kota</Text>
                </View>
                <Image style={{width:90,height:73}} source={require('./image/kendaraan.png')}/>
              </TouchableOpacity>
              <TouchableOpacity style={{width:320,height:45}}/>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
