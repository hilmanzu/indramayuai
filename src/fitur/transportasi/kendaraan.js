import React, {Component} from 'react';
import {ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert,Picker} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import Communications from 'react-native-communications';
import store from 'react-native-simple-store';
const haversine = require('haversine')
import getDirections from 'react-native-google-maps-directions'
import renderIf from './renderIf'
import SearchInput, { createFilter } from 'react-native-search-filter';

const KEYS_TO_FILTERS = ['angkotName'];

export default class kereta extends Component{
  constructor(props){
    super(props);
    this.state = {
      showAlert: false,
      loading:true,
      data :[],
      datas:[],
      category:'',
      searchTerm: ''
      }
    }

  componentWillMount(){
      const { params } = this.props.navigation.state;
      store.get('position').then((res) =>{
        fetch('https://indramayuapp.nusantech.co/v1/transportasiangkot', {
          method: 'GET',
          headers: {  
                'Accept':'application/json',
                'Content-Type':'application/json'}
              })
        .then((response) => response.json())
        .then((responseJson) =>{
          data = responseJson.data
            this.setState({
              data : data,loading:false
            })
        })
        fetch('https://indramayuapp.nusantech.co/v1/transportasiangkot', {
          method: 'GET',
          headers: {  
                'Accept':'application/json',
                'Content-Type':'application/json'}
              })
        .then((response) => response.json())
        .then((responseJson) =>{
          data = responseJson.data
            this.setState({
              datas : data,loading:false
            })
        })
      })
  }

   searchUpdated(term) {
    this.setState({ searchTerm: term })
  }

  render() {
    const { data,loading,datas,category } = this.state
    const { params } = this.props.navigation.state;
    var re = /(<[^>]+>|<[^>]>|<\/[^>]>)/g;
     const filteredEmails = data.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

    if (loading === true) {
        return(
          <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
            <ActivityIndicator
              color={`rgba(0, 163, 192, 1)`}
              size={`large`}
              style={styles.s34e24b9b}
            />
            <Text style={{paddingTop:20}}> Menunggu Sebentar </Text>
          </View>
        )
      }

    return (
      <View style={styles.subcontainer}>
        <Image style={styles.ImageLogo} source={require('../image/back2.png')}/>
        <View style={styles.header}>
        <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>Kendaraan Transportasi</Text>
        </View>
        <View style={styles.const}>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center',marginTop:20}}>
          <View style={{backgroundColor:'#fff',marginTop:20,width:320,height:120,alignItems:'center',elevation:5,borderRadius:5,marginBottom:20,borderWidth: 0.5,borderColor: '#149BE5'}}>
            <Text style={{fontWeight:'bold',width:100,marginTop:10,marginRight:170,marginBottom:5}}>
              Search
            </Text>
            <View style={styles.textinput}>
              <SearchInput 
                onChangeText={(term) => { this.searchUpdated(term) }} 
                style={styles.searchInput}
                placeholder=" "
              />
            </View>
          </View>
            {filteredEmails.map((data)=>
                    <View style={{marginVertical:10,marginHorizontal:2}}>
                      <TouchableOpacity style={{width:320,height:120,backgroundColor:'#fff',borderRadius:5,elevation:5}} onPress={()=>this.props.navigation.navigate('DetailKendaraan',{image:'http://indramayuapp.nusantech.co/uploads/transportasi/' + data.image,rute:data.angkotRute.replace(re,''),nama:data.angkotName,waktu:data.OperationalTime,tips:data.angkotTips.replace(re,'')})}>
                        <Image source={{uri: 'http://indramayuapp.nusantech.co/uploads/transportasi/' + data.image}} style={{position:'absolute',marginLeft:10,width:120,height:100,marginTop:10}}/>
                        <Text numberOfLines={2} style={{marginTop:10,width:180,fontSize:14,fontWeight:'bold',marginLeft:140,color:'#149BE5'}}>{data.angkotName}</Text>
                        <Text style={{width:180,fontSize:14,fontWeight:'bold',marginLeft:140,color:'#000000'}}>Rute</Text>
                        <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                          <Text numberOfLines={1} style={{width:150,fontSize:12}}>{data.angkotRute.replace(re,'')}</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
              )
            }
        </ScrollView>
        </View>
      </View>
    );
  }
}