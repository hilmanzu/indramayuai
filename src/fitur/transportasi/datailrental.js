import React, {Component} from 'react';
import {Dimensions,Picker,ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import Communications from 'react-native-communications';
import store from 'react-native-simple-store';
const haversine = require('haversine')
import getDirections from 'react-native-google-maps-directions'
import HTML from 'react-native-render-html';

const image = 'https://3.bp.blogspot.com/-_Ll9XLApuAA/V9qPPtjS5AI/AAAAAAAAAPk/5qnNrYXaLm8-iACn0l1dYpKGwnqX6LhlACLcB/s1600/Terminal%2BBus%2BIndramayu.jpg'

export default class detail extends Component{

   handleGetDirections(e){
    const data = {
      source: {
        latitude: e.deslatitude,
        longitude: e.deslongitude
      },
      destination: {
        latitude: e.latitude,
        longitude: e.longitude
      },
      params: [
        {
          key: "travelmode",
          value: "driving"        // may be "walking", "bicycling" or "transit" as well
        },
        {
          key: "dir_action",
          value: "navigate"       // this instantly initializes navigation using the given travel mode 
        }
      ]
    }
    getDirections(data)
  }

  render() {
    const {params} = this.props.navigation.state;

    return (
      <View style={styles.subcontainer}>
        <Image style={styles.ImageLogo} source={require('../image/back2.png')}/>
        <View style={styles.header}>
        <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>Detail Rental</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom:10}}>
          <Image source={{uri : params.image}} style={{width:Dimensions.get('window').width,height:170}}/>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontWeight:'bold',fontSize:18,marginTop:10,color:'#000000'}}>
              {params.nama}
          </Text>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontSize:14,marginTop:10,color:'#007CBF'}}>
              {params.alamat}
          </Text>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontWeight:'bold',fontSize:15,marginTop:10,color:'#007CBF',marginBottom:10}}>
              Jenis Mobil
          </Text>
          {params.mobil.map((data)=>
            <View style={{flexDirection:'row'}}>
              <Image source={require('./image/transportasi.png')} style={{width:30,height:30,marginLeft:20}}/>
              <Text numberOfLines={1} style={{marginLeft:10,width:300,fontSize:14,marginTop:2,color:'#000000'}}>
                  {data.MasterMobil.mobilName}
              </Text>
            </View>
          )}
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontWeight:'bold',fontSize:15,marginTop:10,color:'#007CBF'}}>
              Kontak
          </Text>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontSize:14,marginTop:10,color:'#000000'}}>
              {params.kontak}
          </Text>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontWeight:'bold',fontSize:15,marginTop:10,color:'#007CBF'}}>
              Alamat
          </Text>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontSize:14,marginTop:10,color:'#000000'}}>
              {params.alamat}
          </Text>
          <View style={{alignItems:'center',marginTop:20}}>
            <TouchableOpacity style={styles.button} onPress={this.handleGetDirections.bind(this,{latitude:params.latitude,longitude:params.longitude,deslatitude:params.deslatitude,deslongitude:params.deslongitude})}>
              <Text style={styles.textbutton}>Lihat Lokasi</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}