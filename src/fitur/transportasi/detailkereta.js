import React, {Component} from 'react';
import {Dimensions,Picker,ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import Communications from 'react-native-communications';
import store from 'react-native-simple-store';
const haversine = require('haversine')
import getDirections from 'react-native-google-maps-directions'
import HTML from 'react-native-render-html';

const image = 'https://3.bp.blogspot.com/-_Ll9XLApuAA/V9qPPtjS5AI/AAAAAAAAAPk/5qnNrYXaLm8-iACn0l1dYpKGwnqX6LhlACLcB/s1600/Terminal%2BBus%2BIndramayu.jpg'

export default class detail extends Component{

  render() {
    const {params} = this.props.navigation.state;
    var re = /(<[^>]+>|<[^>]>|<\/[^>]>)/g;

    return (
      <View style={styles.subcontainer}>
        <Image style={styles.ImageLogo} source={require('../image/back2.png')}/>
        <View style={styles.header}>
        <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>Detail Kereta</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom:10}}>
          <Image source={{uri : image}} style={{width:Dimensions.get('window').width,height:170}}/>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontWeight:'bold',fontSize:18,marginTop:10,color:'#000000'}}>
              {params.nama}
          </Text>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontWeight:'bold',fontSize:14,marginTop:10,color:'#007CBF'}}>
              {params.rute}
          </Text>
          <View style={{flexDirection:'row',marginTop:5,marginLeft:20,alignItems:'center'}}>
            <Image source={require('../image/clock.png')} style={{width:20,height:20}}/>
            <Text numberOfLines={1} style={{width:300,fontSize:14,color:'#000000',marginLeft:10}}>
              {params.waktu} WIB
            </Text>
          </View>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontWeight:'bold',fontSize:15,marginTop:10,color:'#007CBF'}}>
              Stasiun
          </Text>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontSize:14,marginTop:5,color:'#000000'}}>
              {params.terminal}
          </Text>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontWeight:'bold',fontSize:14,marginTop:15,color:'#007CBF'}}>
              Kota Utama
          </Text>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontSize:14,marginTop:5,color:'#000000'}}>
              {params.utama}
          </Text>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontWeight:'bold',fontSize:15,marginTop:10,color:'#007CBF'}}>
              Alamat
          </Text>
          <Text numberOfLines={1} style={{marginLeft:20,width:300,fontSize:14,marginTop:5,color:'#000000'}}>
              {params.alamat.replace(re,'')}
          </Text>
        </ScrollView>
      </View>
    );
  }
}