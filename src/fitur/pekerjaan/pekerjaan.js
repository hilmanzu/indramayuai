import React, {Component} from 'react';
import {Dimensions,Picker,ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import Communications from 'react-native-communications';
import store from 'react-native-simple-store';
const haversine = require('haversine')
import getDirections from 'react-native-google-maps-directions'
import renderIf from './renderIf'

export default class pekerjaan extends Component{
  constructor(props){
    super(props);
    this.state = {
      loading    : true,
      data       : [],
      category   : '',
      pendidikan : '',
      ccategory  : '',
      cpendidikan: '',
      exp : []
      }
    }

  componentDidMount(){

        fetch('https://indramayuapp.nusantech.co/v1/kategoripendidikanloker')
        .then((response) => response.json())
        .then((responseJson) =>{
          Pendidikan = responseJson.data
          console.log(Pendidikan)
            this.setState({
              pendidikan : Pendidikan
            })

          fetch('https://indramayuapp.nusantech.co/v1/kategoriloker')
          .then((response) => response.json())
          .then((responseJson) =>{
            Kategori = responseJson.data
            console.log(Kategori)
              this.setState({
                category : Kategori
              })

            fetch('https://indramayuapp.nusantech.co/v1/loker')
            .then((response) => response.json())
            .then((responseJson) =>{
              var data = responseJson.data
                this.setState({
                  data : data, loading:false
                })
            })

            fetch('https://indramayuapp.nusantech.co/v1/lokerunexp')
            .then((response) => response.json())
            .then((responseJson) =>{
              var data = responseJson.data
                this.setState({
                  exp : data,
                })
            })

          })

        })

  }

  render() {
    const { loading,category,pendidikan,ccategory,cpendidikan,exp } = this.state
    let data = this.state.data
    var filter = data.filter((item) => item.KategoriLoker.id === ccategory && item.KategoriPendidikanLoker.id === cpendidikan)

    if (loading === true) {
        return(
          <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
            <ActivityIndicator
              color={`rgba(0, 163, 192, 1)`}
              size={`large`}
              style={styles.s34e24b9b}
            />
            <Text style={{paddingTop:20}}> Menunggu Sebentar </Text>
          </View>
        )
      }

    return (
    <ImageBackground style={styles.ImageBackground} source={require('../image/back.png')}>
      <View style={{flex: 1}}>
        <Image style={styles.ImageLogo} source={require('../image/back2.png')}/>
        <View style={{width:Dimensions.get('window').width,height:50,backgroundColor:'#149BE5',alignItems:'center',flexDirection:'row'}}>
          <View style={{zIndex: 1,flex:1}}>
            <TouchableOpacity onPress={() => {this.props.navigation.openDrawer()}}>
              <Image source={require('../image/menu.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20}}/>
            </TouchableOpacity>
          </View>
          <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',flex:2,width:200}}>Lowongan Pekerjaan</Text>
          <View style={{zIndex: 1,flex:1}}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('Arsip')}>
              <Image source={require('../image/writing.png')} style={{marginLeft:50,width:20,height:20,tintColor:'#fff'}}/>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.const}>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center',marginTop:5,marginBottom:10}}>
        <View style={{backgroundColor:'#fff',marginTop:20,width:320,height:195,alignItems:'center',elevation:5,borderRadius:5}}>
          <Text style={{fontWeight:'bold',width:100,marginTop:20,marginRight:170,marginBottom:10}}>
            Kategori
          </Text>
          <View style={styles.itempekerjaan}>
            <Picker
              selectedValue={this.state.ccategory}
              onValueChange={(ccategory) => this.setState({ccategory})}>
              <Picker.Item label='Pilih Kategori' value={null} />
              {category.map((item, index) => {
              return (<Picker.Item label={item.kategoriName} value={item.id} key={item.id}/>) 
              })}
              <Picker.Item label='Expired' value={'exp'}/>
            </Picker>
          </View>
          <Text style={{fontWeight:'bold',width:100,marginVertical:10,marginRight:170}}>
            Pendidikan
          </Text>
          <View style={styles.itempekerjaan}>
            <Picker
              selectedValue={this.state.cpendidikan}
              onValueChange={(cpendidikan) => this.setState({cpendidikan})}>
              <Picker.Item label='Pilih Pendidikan Minimum' value={null} />
              {pendidikan.map((item, index) => {
              return (<Picker.Item label={item.kategoriName} value={item.id} key={item.id}/>) 
              })}
            </Picker>
          </View>
        </View>
          {filter.map((data)=>
            <View style={{marginVertical:10,marginHorizontal:2}}>
              <TouchableOpacity style={{width:320,height:90,backgroundColor:'#fff',borderRadius:5,elevation:5,justifyContent:'center'}} onPress={()=>this.props.navigation.navigate('DetailPekerjaan',{pekerjaan : data.lokerName,perusahaan:data.lokerPerusahaan,batas:data.lokerEndDate,deskripsi:data.lokerDescription,posisi:data.lokerPosisi,kebutuhan:data.lokerKebutuhan,kualifikasi:data.lokerKualifikasi})}>
                <Text numberOfLines={1} style={{color:'#149BE5',fontSize:14,fontWeight:'bold',marginLeft:20}}>{data.lokerName}</Text>
                <Text numberOfLines={1} style={{fontSize:12,marginTop:5,marginLeft:20}}>{data.lokerPerusahaan}</Text>
                <Text numberOfLines={1} style={{fontSize:12,marginLeft:20}}>Batas Waktu :{data.lokerEndDate}</Text>
              </TouchableOpacity>
            </View>
            )
          }
          {exp.map((data)=>
            <View style={{marginVertical:10,marginHorizontal:2}}>
              {renderIf(ccategory == 'exp' ? true : false)(
              <TouchableOpacity style={{width:320,height:90,backgroundColor:'#fff',borderRadius:5,elevation:5,justifyContent:'center'}} onPress={()=>this.props.navigation.navigate('DetailPekerjaan',{pekerjaan : data.lokerName,perusahaan:data.lokerPerusahaan,batas:data.lokerEndDate,deskripsi:data.lokerDescription,posisi:data.lokerPosisi,kebutuhan:data.lokerKebutuhan,kualifikasi:data.lokerKualifikasi})}>
                <Text numberOfLines={1} style={{color:'#149BE5',fontSize:14,fontWeight:'bold',marginLeft:20}}>{data.lokerName}</Text>
                <Text numberOfLines={1} style={{fontSize:12,marginTop:5,marginLeft:20}}>{data.lokerPerusahaan}</Text>
                <Text numberOfLines={1} style={{fontSize:12,marginLeft:20}}>Batas Waktu :{data.lokerEndDate}</Text>
              </TouchableOpacity>
              )}
            </View>
            )
          }
        </ScrollView>
      </View>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          message="Harap Menunggu"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
      </View>
    </ImageBackground>
    );
  }
}