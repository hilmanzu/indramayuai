import React, {Component} from 'react';
import {Picker,ScrollView,ActivityIndicator,Image,Dimensions,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../style/style'
import Pbar from '../style/pbar'
import store from 'react-native-simple-store';
import AwesomeAlert from 'react-native-awesome-alerts';

export default class Pengaduan extends Component{

  constructor(props){
    super(props);
    this.state={
      data: '',
      loading:true,
      showAlert:false,
      TopikId:''
    }  
  }

  componentDidMount(){
    store.get('id').then((res) =>{
      fetch( 'https://indramayuapp.nusantech.co/v1/JenisPendidikan')
      .then((response) => response.json())
      .then((responseJson) =>{
          this.setState({
            data: responseJson.data , loading:false
          })
      })
        .catch((error) =>{
          console.error(error);
      })
    })
  }

  render() {  
    const {data,loading,TopikId,showAlert} = this.state;
    console.log(TopikId)

    if (loading === true) {
        return(
          <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
            <ActivityIndicator
              color={`rgba(0, 163, 192, 1)`}
              size={`large`}
              style={styles.s34e24b9b}
            />
            <Text style={{paddingTop:20}}> Menunggu Sebentar </Text>
          </View>
        )
      }

    return (
      <ImageBackground style={styles.ImageBackground} source={require('../image/back.png')}>
        <View style={{flex: 1,alignItems:'center'}}>
          <Image style={styles.ImageLogo} source={require('../image/back2.png')}/>
          <View style={styles.header}>
            <View style={{position:'absolute',zIndex: 1}}>
              <TouchableOpacity onPress={() => {this.props.navigation.openDrawer()}}>
                <Image source={require('../image/menu.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
              </TouchableOpacity>
            </View>
            <Text style={styles.headertext}>Lowongan Kerja</Text>
          </View>
          <View style={{borderRadius:10,elevation:5,marginTop:50,height:200,width:320,backgroundColor:'#fff',paddingLeft:10,paddingRight:10,alignItems:'center',justifyContent:'center'}}>
                <TouchableOpacity style={{marginTop:10,flexDirection:'row',width:280,height:55,backgroundColor:'#149BE5',borderRadius:10,alignItems:'center'}} onPress={()=>this.props.navigation.navigate('Pekerjaan')}>
                  <Image style={{width:20,height:20,tintColor:'#fff',marginHorizontal:15}} source={require('../image/Loker.png')}/>
                  <Text style={{color:'#fff',textAlign:'center',fontSize:15,fontWeight:'bold',width:180}}>Lowongan Kerja</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{marginTop:10,flexDirection:'row',width:280,height:55,backgroundColor:'#149BE5',borderRadius:10,alignItems:'center'}} onPress={()=>this.props.navigation.navigate('Kuning')}>
                  <Image style={{width:20,height:20,tintColor:'#fff',marginHorizontal:15}} source={require('../image/kk2.png')}/>
                  <Text style={{color:'#fff',textAlign:'center',fontSize:15,fontWeight:'bold',width:180}}>Kartu Kuning</Text>
                </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    );
  }
}