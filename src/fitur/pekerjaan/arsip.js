import React, {Component} from 'react';
import {Picker,ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import Communications from 'react-native-communications';
import store from 'react-native-simple-store';
const haversine = require('haversine')
import getDirections from 'react-native-google-maps-directions'
import renderIf from './renderIf'

export default class pekerjaan extends Component{
  constructor(props){
    super(props);
    this.state = {
      loading    : true,
      data       : [],
      category   : '',
      pendidikan : '',
      ccategory  : '',
      cpendidikan: '',
      exp : []
      }
    }

  componentDidMount(){

        fetch('https://indramayuapp.nusantech.co/v1/kategoripendidikanloker')
        .then((response) => response.json())
        .then((responseJson) =>{
          Pendidikan = responseJson.data
          console.log(Pendidikan)
            this.setState({
              pendidikan : Pendidikan
            })

          fetch('https://indramayuapp.nusantech.co/v1/kategoriloker')
          .then((response) => response.json())
          .then((responseJson) =>{
            Kategori = responseJson.data
            console.log(Kategori)
              this.setState({
                category : Kategori
              })

            fetch('https://indramayuapp.nusantech.co/v1/loker')
            .then((response) => response.json())
            .then((responseJson) =>{
              var data = responseJson.data
                this.setState({
                  data : data,
                })
            })

            fetch('https://indramayuapp.nusantech.co/v1/lokerarsip')
            .then((response) => response.json())
            .then((responseJson) =>{
              var data = responseJson.data
                this.setState({
                  exp : data, loading:false
                })
            })

          })

        })

  }

  render() {
    const { loading,category,pendidikan,ccategory,cpendidikan,exp } = this.state
    let data = this.state.data
    var filter = data.filter((item) => item.KategoriLoker.id === ccategory && item.KategoriPendidikanLoker.id === cpendidikan)

    if (loading === true) {
        return(
          <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
            <ActivityIndicator
              color={`rgba(0, 163, 192, 1)`}
              size={`large`}
              style={styles.s34e24b9b}
            />
            <Text style={{paddingTop:20}}> Menunggu Sebentar </Text>
          </View>
        )
      }

    return (
    <ImageBackground style={styles.ImageBackground} source={require('../image/back.png')}>
      <View style={{flex: 1}}>
        <Image style={styles.ImageLogo} source={require('../image/back2.png')}/>
        <View style={styles.header}>
          <View style={{position:'absolute',zIndex: 1}}>
            <TouchableOpacity onPress={() => {this.props.navigation.goBack()}}>
              <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
            </TouchableOpacity>
          </View>
          <Text style={styles.headertext}>Arsip</Text>
        </View>
        <View style={styles.const}>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center',marginTop:5,marginBottom:10}}>
          {exp.map((data)=>
            <View style={{marginVertical:10,marginHorizontal:2}}>
              <TouchableOpacity style={{width:320,height:90,backgroundColor:'#fff',borderRadius:5,elevation:5,justifyContent:'center'}} onPress={()=>this.props.navigation.navigate('DetailPekerjaan',{pekerjaan : data.lokerName,perusahaan:data.lokerPerusahaan,batas:data.lokerEndDate,deskripsi:data.lokerDescription,posisi:data.lokerPosisi,kebutuhan:data.lokerKebutuhan,kualifikasi:data.lokerKualifikasi})}>
                <Text numberOfLines={1} style={{color:'#149BE5',fontSize:14,fontWeight:'bold',marginLeft:20}}>{data.lokerName}</Text>
                <Text numberOfLines={1} style={{fontSize:12,marginTop:5,marginLeft:20}}>{data.lokerPerusahaan}</Text>
                <Text numberOfLines={1} style={{fontSize:12,marginLeft:20}}>Batas Waktu :{data.lokerEndDate}</Text>
              </TouchableOpacity>
            </View>
            )
          }
        </ScrollView>
      </View>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          message="Harap Menunggu"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
      </View>
    </ImageBackground>
    );
  }
}