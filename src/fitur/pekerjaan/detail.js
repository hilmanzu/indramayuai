import React, {Component} from 'react';
import {Dimensions,Picker,ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import Communications from 'react-native-communications';
import store from 'react-native-simple-store';
const haversine = require('haversine')
import getDirections from 'react-native-google-maps-directions'
import HTML from 'react-native-render-html';

export default class detail extends Component{

  render() {
    const {params} = this.props.navigation.state;

    return (
      <View style={styles.subcontainer}>
        <Image style={styles.ImageLogo} source={require('../image/back2.png')}/>
        <View style={styles.header}>
        <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>Detail Pekerjaan</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} style={{marginHorizontal:20,marginTop:10,marginBottom:10}}>
          <View style={styles.const}>
            <View style={{backgroundColor:'#fff',margin:20,width:320,height:100,justifyContent:'center',elevation:5,borderRadius:10,borderWidth: 0.5,borderColor: '#149BE5'}}>
              <Text numberOfLines={1} style={{width:300,fontWeight:'bold',marginTop:5,marginLeft:20,color:'#007CBF'}}>
                {params.pekerjaan}
              </Text>
              <Text numberOfLines={1} style={{width:100,marginTop:5,marginLeft:20,width:300}}>
                {params.perusahaan}
              </Text>
              <Text numberOfLines={1} style={{width:100,marginTop:5,marginLeft:20,width:300}}>
                Batas Pendaftaran : {params.batas}
              </Text>
            </View>
          </View>

          <View style={{backgroundColor:'#fff',elevation:5,borderRadius:5,margin:5,padding:10,borderWidth: 0.5,borderColor: '#149BE5'}}>
            <Text numberOfLines={1} style={{width:300,fontWeight:'bold',width:100,marginTop:10,color:'#007CBF'}}>
                Deskripsi
            </Text>
            <HTML html={params.deskripsi} imagesMaxWidth={Dimensions.get('window').width}/>

            <Text numberOfLines={1} style={{width:300,fontWeight:'bold',width:100,marginTop:10,color:'#007CBF'}}>
                Posisi
            </Text>
            <HTML html={params.posisi} imagesMaxWidth={Dimensions.get('window').width}/>

            <Text numberOfLines={1} style={{width:300,fontWeight:'bold',width:100,marginTop:10,color:'#007CBF'}}>
                Kebutuhan
            </Text>
            <HTML html={params.kebutuhan} imagesMaxWidth={Dimensions.get('window').width}/>

            <Text numberOfLines={1} style={{width:300,fontWeight:'bold',width:100,marginTop:10,color:'#007CBF'}}>
                Kualifikasi
            </Text>
            <HTML html={params.kualifikasi} imagesMaxWidth={Dimensions.get('window').width}/>
          </View>
        
        </ScrollView>
      </View>
    );
  }
}