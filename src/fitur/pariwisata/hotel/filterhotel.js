import React, {Component} from 'react';
import {Dimensions,ImageBackground,Picker,ScrollView,TextInput,Image,TouchableOpacity,Platform, StyleSheet, Text, View} from 'react-native';
import styles from '../style'
import Stars from 'react-native-stars';
import renderIf from './renderIf'
import Modal from "react-native-modal";
import list from './list.json'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

const image = 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg'
const radio_props = [
  {label: 'Bintang 1', value: 0 },
  {label: 'Bintang 2', value: 1 },
  {label: 'Bintang 3', value: 2 },
  {label: 'Bintang 4', value: 3 },
  {label: 'Bintang 5', value: 4 }
];

export default class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      data :[],
      modal:false,
      modal2:false,
      TopikId :0,
      value : 0
      }
    }


   componentDidMount(){
      const {TopikId} = this.state
      const {params} = this.props.navigation.state;
      fetch( 'https://indramayuapp.nusantech.co/v1/pariwisatahotel' )
      .then((response) => response.json())
      .then((responseJson) =>{
        var filter = responseJson.data.filter((item) => item.hotelStar === params.filter)
          this.setState({
            data: filter , loading:false,
          })
      })
        .catch((error) =>{
          console.error(error);
      })
  }

  normal = () =>
    this.setState({ normal: !this.state.normal });

  render() {
    const {data,TopikId,sorting} = this.state
    var re = /(<[^>]+>|<[^>]>|<\/[^>]>)/g;


    return (
      <View style={styles.container}>

        <View style={styles.header}>
          <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.textHeader}>Hotel</Text>
        </View>

        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.content}>
          {data.map((data,index) =>
            <View style={styles.marginVertical}>
              <TouchableOpacity style={{width:320,height:120,backgroundColor:'#fff',borderRadius:5,elevation:5}} onPress={()=>this.props.navigation.navigate('DetailHotel',{id:data.id})}>
                <Image source={{uri: 'http://indramayuapp.nusantech.co/uploads/pariwisata/' + data.image}} style={{position:'absolute',marginLeft:10,width:120,height:100,marginTop:10}}/>
                <Text numberOfLines={2} style={{marginTop:10,width:180,fontSize:14,fontWeight:'bold',marginLeft:140,color:'#149BE5'}}>{data.hotelName}</Text>
                {renderIf(data.hotelStar == '1' ? true : false)(
                  <View style={{flexDirection:'row',alignItems:'center',marginVertical:5,marginLeft:140}}>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                  </View>
                )}
                {renderIf(data.hotelStar == '2' ? true : false)(
                  <View style={{flexDirection:'row',alignItems:'center',marginVertical:5,marginLeft:140}}>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                  </View>
                )}
                {renderIf(data.hotelStar == '3' ? true : false)(
                  <View style={{flexDirection:'row',alignItems:'center',marginVertical:5,marginLeft:140}}>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                  </View>
                )}
                {renderIf(data.hotelStar == '4' ? true : false)(
                  <View style={{flexDirection:'row',alignItems:'center',marginVertical:5,marginLeft:140}}>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                  </View>
                )}
                {renderIf(data.hotelStar == '5' ? true : false)(
                  <View style={{flexDirection:'row',alignItems:'center',marginVertical:5,marginLeft:140}}>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                      <Image source={require('./image/star.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                  </View>
                )}
                {renderIf(data.hotelStar == 'Melati 3' ? true : false)(
                  <View style={{flexDirection:'row',alignItems:'center',marginVertical:5,marginLeft:140}}>
                    <Text>{data.hotelStar}</Text>
                  </View>
                )}
                <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                   <Text numberOfLines={1} style={{width:150,fontSize:12}}>{data.hotelAlamat.replace(re,'')}</Text>
                 </View>
              </TouchableOpacity>
            </View>
          )}
        </ScrollView>

      </View>
    );
  }
}
