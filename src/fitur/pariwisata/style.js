import {Dimensions,Platform, StyleSheet, Text, View} from 'react-native';

const styles = StyleSheet.create({

  logo: { height: 35, width: 212 ,marginVertical:20},
  icon: {
    width: 20,
    height: 20,
  },
  ////Paging
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
  content: {
    flexGrow : 1,
    backgroundColor: '#fafafa',
    alignItems:'center'
  },
  contentVertical: {
    backgroundColor: '#FFF',
    paddingHorizontal:5
  },
  contentItem: {
    flex: 1,
    backgroundColor: `#ffffff`,
    alignItems: `center`,
    justifyContent: `center`
  },
  ///Header
  header:{
    width:Dimensions.get('window').width,
    height:50,
    justifyContent:'center',
    backgroundColor:'#149BE5'
  },
  textHeader:{
    color:'#fff',
    fontWeight:'bold',
    textAlign:'center'
  },
  ///button pakai touchableopacity
  button:{
    borderRadius:10,
    backgroundColor:'#149BE5',
    height:46,
    width:150,
    justifyContent:'center',
    elevation:1
  },
  textButton:{
    color:'#FFFFFF',
    fontSize: 15,
    textAlign:'center',
    fontWeight:'bold'
  },
  marginVertical:{
    marginVertical:10,
  },
  ///button category
  categoryButton:{
    width:120,
    height:130,
    alignItems:'center',
    justifyContent:'center',
    elevation:3,
    borderRadius:5,
    marginHorizontal:5
  },
  categoryImage:{
    width:80,
    height:60,
    borderRadius:10
  },
  categoryText:{
    width:90,
    fontWeight:'bold',
    color:'#2196f3',
    textAlign:'center',
    marginTop:3
  },
  marginVerticalButton:{
    marginVertical:30
  },
  ///button list
  listButton:{
    width:320,
    height:90,
    elevation:1,
    borderRadius:10,
    flexDirection:'row'
  },
  listImage:{
    width:50,
    height:50,
    marginLeft:30,
    marginTop:23,
    borderRadius:10
  },
  listTextName:{
    fontWeight:'bold',
    color:'#2196f3',
    marginTop:22,
    marginLeft:10,
    width:200
  },
  listTextDeskripsi:{
    color:'#000000',
    marginLeft:10,
    width:200
  },
  /////input
  Text:{
    fontSize:14,
    marginLeft:20
  },
  input:{
    backgroundColor:'#F2FBFF',
    borderColor: '#149BE5',
    borderRadius:10,
    borderWidth: 1,
    height:40,
    width:280,
    marginHorizontal:20,
    marginTop:15
  },
  inputText:{
    fontSize:14,
    marginHorizontal:10
  },
  ////picker
  picker:{
    backgroundColor:'#F2FBFF',
    borderColor: '#149BE5',
    borderRadius:10,
    borderWidth: 1,
    height:40,
    width:280,
    marginHorizontal:20,
    marginTop:15,
    justifyContent:'center'
  }

});

export default styles 
