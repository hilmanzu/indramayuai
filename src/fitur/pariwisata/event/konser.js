import React, {Component} from 'react';
import {ImageBackground,Picker,ScrollView,TextInput,Image,TouchableOpacity,Platform, StyleSheet, Text, View} from 'react-native';
import styles from '../style'

const image = 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg'

export default class App extends Component {

  static navigationOptions = {
    headerStyle: {
          position: 'absolute',
          top: 0,
          left: 0
        },
      tabBarLabel: 'Konser',
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={require('./ticket.png')}
          style={[{width:20,height:20}, { tintColor: tintColor }]}
        />
      ),
    };

  constructor(props){
    super(props);
    this.state = {
      data :[]
      }
    }


   componentDidMount(){
      fetch( 'https://indramayuapp.nusantech.co/v1/pariwisataevent' )
      .then((response) => response.json())
      .then((responseJson) =>{
          this.setState({
            data: responseJson.data , loading:false
          })
      })
        .catch((error) =>{
          console.error(error);
      })
  }

  render() {
    const {data} = this.state
    var re = /(<[^>]+>|<[^>]>|<\/[^>]>)/g;
    var filter = data.filter((item) => item.KategoriEvent.kategoriName === 'Konser')

    return (
      <View style={styles.container}>

        <View style={styles.header}>
          <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.textHeader}>Event Pariwisata</Text>
        </View>

        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.content}>
          {filter.map((data,index) =>
            <View style={styles.marginVertical}>
              <TouchableOpacity style={{width: 320,height: 150,elevation:1,borderRadius: 5,flexDirection:'row',justifyContent:'center',alignItems:'center'}} onPress={()=>this.props.navigation.navigate('DetailEvent',{id:data.id})}>
                <View>
                  <Image style={{width:70,height:70}} source={require('./item.png')}/>
                </View>
                <View>
                  <Text numberOfLines={2} style={{fontSize:14,fontWeight:'bold',color:'#0000000',marginTop: 5,marginLeft:10,width:200}}>{data.eventName}</Text>
                  <Text numberOfLines={1} style={{fontSize:14,fontWeight:'bold',color:'#0000000',marginTop: 5,marginLeft:10,width:200}}>Mulai {data.eventDate}</Text>
                </View>
              </TouchableOpacity>
            </View>
          )}

        </ScrollView>

      </View>
    );
  }
}
