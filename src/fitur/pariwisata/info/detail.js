import React, {Component} from 'react';
import {Dimensions,Picker,ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from './style'
import AwesomeAlert from 'react-native-awesome-alerts';
import Communications from 'react-native-communications';
import store from 'react-native-simple-store';
const haversine = require('haversine')
import getDirections from 'react-native-google-maps-directions'
import HTML from 'react-native-render-html';

export default class detail extends Component{

  render() {
    const {params} = this.props.navigation.state;

    return (
      <View style={styles.subcontainer}>
        <View style={styles.header}>
        <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>Info Pariwisata Indramayu</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} style={{marginHorizontal:20,marginTop:10,marginBottom:10}}>
          
          <Image source={{uri:params.gambar}} style={{width:320,height:150}}/>
          <Text numberOfLines={1} style={{width:300,fontWeight:'bold',marginTop:10,color:'#149BE5',marginBottom:5}}>
            {params.judul}
          </Text>
          <Text numberOfLines={1} style={{fontSize:12,width:200}}>
            {params.waktu}
          </Text>
          <HTML style={{marginBottom:15}} html={params.deskripsi} imagesMaxWidth={Dimensions.get('window').width}/>
        
        </ScrollView>
      </View>
    );
  }
}