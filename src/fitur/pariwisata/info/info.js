import React, { Component } from 'react';
import { BackHandler,AppRegistry,View,StyleSheet,ListView,Dimensions,Image,ImageBackground,TouchableOpacity,ScrollView } from 'react-native';
import { Container,Text, Header, Content, Item,Separator, Input,Button, Icon ,Label,Card,CardItem,Left,List, ListItem,Form, Body, Right,Thumbnail} from 'native-base';
import store from 'react-native-simple-store';
import moment from 'moment'
import Modal from "react-native-modal";
import HTML from 'react-native-render-html';
import renderIf from './renderIf'


export default class news extends Component{

  constructor(){
    super();
    this.state={
      pesan :'',
      dataSource: [],
      data:'',
      loading:true,
      judul:'',
      gambar:'',
      deskripsi:'',
      waktu:'',
    }  
  }

  componentDidMount(){
      fetch('https://indramayuapp.nusantech.co/v1/pariwisata')
      .then((response) => response.json())
      .then((responseJson) =>{
        console.log(responseJson)
        data = responseJson.data; // here we have all products data
        this.setState({
          dataSource: data,
          data:data[0]
        })
      })
        .catch((error) =>{
          console.error(error);
      })
  }

  _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });

  renderPage(image, index) {
          return (
              <View key={index}>
                  <Image style={{ width: BannerWidth, height: BannerHeight }} source={{ uri: image }} />
              </View>
          );
      }

  render(){
    const { dataSource,loading,data} = this.state;
    const { navigate } = this.props.navigation;

    return(
        <Container>
          <View style={styles.header}>
          <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
            <Text style={styles.headertext}>Info Pariwisata</Text>
          </View>
          <Content contentContainerStyle={{alignItems:'center'}}>
                <TouchableOpacity  onPress={()=>this.props.navigation.navigate('DetailInfo',{waktu:moment(data.createdAt).format('DD,MMMM YYYY'), gambar: 'http://indramayuapp.nusantech.co/uploads/pariwisata/' + data.image,judul:data.pariwisataName,deskripsi:data.pariwisataDescription})} style={{padding:5,width:320,elevation:5,height:180,backgroundColor:'#fff',marginTop:10,alignItems:'center',justifyContent:'center',borderWidth: 0.5,borderColor: '#149BE5'}}>
                  <Text style={{color:'#149BE5',fontSize:15,fontWeight:'bold'}} numberOfLines={2}>{data.pariwisataName}</Text>
                  <Image style={{width:300,height:100,marginTop:5}} source={{uri:"http://indramayuapp.nusantech.co/uploads/pariwisata/" + data.image}}/>
                </TouchableOpacity>
                {dataSource.map((data,index)=>
                    <TouchableOpacity style={{ marginHorizontal:10,backgroundColor:'#fff',flexDirection:'row',padding:10}}  onPress={()=>this.props.navigation.navigate('DetailInfo',{waktu:moment(data.createdAt).format('DD,MMMM YYYY'), gambar: 'http://indramayuapp.nusantech.co/uploads/pariwisata/' + data.image,judul:data.pariwisataName,deskripsi:data.pariwisataDescription})}>
                      <Image source={{uri: 'http://indramayuapp.nusantech.co/uploads/pariwisata/' + data.image}} style={{width:85,height:70}}/>
                      <View>
                        <Text numberOfLines={3} style={{fontSize:15,color:'#149BE5',marginLeft:10,width:200}}>{data.pariwisataName}</Text>
                        <Text note numberOfLines={1} style={{fontSize:8,marginLeft:10,width:200}}>{moment(data.createdAt).format('DD MMMM YYYY')} WIB</Text>
                      </View>
                    </TouchableOpacity>
                )}
          </Content>             
        </Container>
    );
  }
}

const styles = StyleSheet.create({
  Text:{
  fontFamily: "Quicksand-Bold",
  fontStyle: "normal",
  fontWeight: "normal",
  fontSize: 20,
  marginBottom:50,
  textAlign: "center",
  color: "#f57f17",
  textShadowColor: "0px 4px 4px rgba(0, 0, 0, 0.25)"
},
Text2:{
      fontFamily: "quicksand",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: 18,
      textAlign: "center",
      color: "#E0E0E0",
      marginLeft:20,
      textShadowColor: "0px 4px 4px rgba(0, 0, 0, 0.25)"
    },
    icon3: {
    width: 20,
    height: 20,
    },
  ImageLogo:{
    height:170,position:'absolute',width: Dimensions.get('window').width,
  },
  header:{
    width:Dimensions.get('window').width,
    height:50,
    justifyContent:'center',
    backgroundColor:'#149BE5',
  },
  headertext:{
    color:'#fff',
    textAlign:'center',
  },
});
