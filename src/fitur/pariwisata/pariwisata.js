import React, {Component} from 'react';
import {Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../style/style'
import store from 'react-native-simple-store';
import {PermissionsAndroid} from 'react-native';
import Carousel from 'react-native-snap-carousel';

const { width: viewport } = Dimensions.get('window')
const sliderWidth1 = wp(100)
const itemHorizontalMargin1 = wp(0.36)
const sliderWidth = viewport
const itemWidth = (sliderWidth1 + (itemHorizontalMargin1 * 2)) - 15

function wp(percentage){
  const value = (percentage * viewport) / 100
  return Math.round(value)
}

export default class pariwisata extends Component{

  constructor(props) {
    super(props)
      this.state = {
        dataSource: '',
        loading: true,
        jadwal:'',
        data:[]
      }
  }

  componentDidMount(){
    fetch( 'https://indramayuapp.nusantech.co/v1/banner')
      .then((response) => response.json())
      .then((responseJson) =>{
          this.setState({
            data: responseJson.data , loading:false
          })
      })
        .catch((error) =>{
          console.error(error);
      })
  }

  _renderItem ({item, index}) {
        return (
            <View style={{height:150,width:320,marginTop:10}}>
                <Image source={{uri: 'https://indramayuapp.nusantech.co/uploads/banner/' + item.image}} style={{position:'absolute',width:Dimensions.get('window').width,height:150}}/>
            </View>
        );
    }


  render() {
    const { navigate } = this.props.navigation;
    const { data } = this.state
    
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={{position:'absolute',zIndex: 1}}>
            <TouchableOpacity onPress={() => {this.props.navigation.openDrawer()}}>
              <Image source={require('../image/menu.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
            </TouchableOpacity>
          </View>
          <Text style={styles.headertext}>Pariwisata</Text>
        </View>
        <View style={{alignItems:'center'}}>
            <Image source={require('../image/back2.png')} style={styles.ImageLogo2}/>
             <Carousel
              ref={(c) => { this._carousel = c; }}
              data={data}
              autoplay = {true}
              loop = {true}
              renderItem={this._renderItem}
              sliderWidth={sliderWidth}
              itemWidth={itemWidth}
            />
          <View style={{position:'absolute',marginTop:170}}>
            <View style={{marginTop:3}}>
              <TouchableOpacity style={{borderWidth: 0.5,borderColor: '#149BE5',backgroundColor:'#FFFFFF',elevation: 5,width:320,height:60,marginTop:10,borderRadius:5,justifyContent:'center'}} onPress={() => this.props.navigation.navigate('Info')}>
                <Text style={{fontSize:15,marginLeft:20,color:'#149BE5',fontWeight:'bold'}}>Info</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{borderWidth: 0.5,borderColor: '#149BE5',backgroundColor:'#FFFFFF',elevation: 5,width:320,height:60,marginTop:10,borderRadius:5,justifyContent:'center'}} onPress={() => this.props.navigation.navigate('Event')}>
                <Text style={{fontSize:15,marginLeft:20,color:'#149BE5',fontWeight:'bold'}}>Event</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{borderWidth: 0.5,borderColor: '#149BE5',backgroundColor:'#FFFFFF',elevation: 5,width:320,height:60,marginTop:10,borderRadius:5,justifyContent:'center'}} onPress={() => this.props.navigation.navigate('Hotel')}>
                <Text style={{fontSize:15,marginLeft:20,color:'#149BE5',fontWeight:'bold'}}>Hotel</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{borderWidth: 0.5,borderColor: '#149BE5',backgroundColor:'#FFFFFF',elevation: 5,width:320,height:60,marginTop:10,borderRadius:5,justifyContent:'center'}} onPress={() => this.props.navigation.navigate('Destinasi')}>
                <Text style={{fontSize:15,marginLeft:20,color:'#149BE5',fontWeight:'bold'}}>Destinasi Wisata</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{borderWidth: 0.5,borderColor: '#149BE5',backgroundColor:'#FFFFFF',elevation: 5,width:320,height:60,marginTop:10,borderRadius:5,justifyContent:'center'}} onPress={() => this.props.navigation.navigate('Kuliner')}>
                <Text style={{fontSize:15,marginLeft:20,color:'#149BE5',fontWeight:'bold'}}>Kuliner</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
