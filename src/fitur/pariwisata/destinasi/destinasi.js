import React, {Component} from 'react';
import {Dimensions,ImageBackground,Picker,ScrollView,TextInput,Image,TouchableOpacity,Platform, StyleSheet, Text, View} from 'react-native';
import styles from '../style'
import Stars from 'react-native-stars';
import renderIf from './renderIf'
import Modal from "react-native-modal";
import list from './list.json'
import store from 'react-native-simple-store';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

export default class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      data :[],
      datas:[],
      modal:false,
      modal2:false,
      TopikId :'',
      value : 0
      }
    }


   componentDidMount(){

      store.get('position').then((res) =>{
        fetch('https://indramayuapp.nusantech.co/v1/pariwisatadestinasi', {
          method: 'GET',
          headers: {  
                'Accept':'application/json',
                'Content-Type':'application/json'}
              })
        .then((response) => response.json())
        .then((responseJson) =>{
          data = responseJson.data
            this.setState({
              data : data,loading:false
            })
        })
        fetch('https://indramayuapp.nusantech.co/v1/kategoridestinasi', {
          method: 'GET',
          headers: {  
                'Accept':'application/json',
                'Content-Type':'application/json'}
              })
        .then((response) => response.json())
        .then((responseJson) =>{
          data = responseJson.data
            this.setState({
              datas : data,loading:false
            })
        })
      })

  }

  render() {
    const {data,TopikId,sorting,datas,category} = this.state
    var re = /(<[^>]+>|<[^>]>|<\/[^>]>)/g;
    var filter = data.filter((item) => item.KategoriDestinasi.kategoriName === this.state.TopikId)


    return (
      <View style={styles.container}>

        <View style={styles.header}>
          <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.textHeader}>Destinasi Pariwisata</Text>
        </View>

        {renderIf(TopikId == '' ? true : false)(
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.content}>
            {data.map((data,index) =>
              <View style={styles.marginVertical}>
                <TouchableOpacity style={{width:320,height:120,backgroundColor:'#fff',borderRadius:5,elevation:5}} onPress={()=>this.props.navigation.navigate('DetailDestinasi',{id:data.id})}>
                  <Image source={{uri: 'http://indramayuapp.nusantech.co/uploads/pariwisata/' + data.image}} style={{position:'absolute',marginLeft:10,width:120,height:100,marginTop:10}}/>
                  <Text numberOfLines={2} style={{marginTop:10,width:180,fontSize:14,fontWeight:'bold',marginLeft:140,color:'#149BE5'}}>{data.KategoriDestinasi.kategoriName}</Text>    
                  <Text numberOfLines={2} style={{marginTop:10,width:180,fontSize:14,fontWeight:'bold',marginLeft:140,color:'#000000'}}>{data.destinasiName}</Text>
                  <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                     <Text numberOfLines={1} style={{width:150,fontSize:12}}>{data.OperationalTime}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          </ScrollView>
        )}

       {renderIf(TopikId !== '' ? true : false)(
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.content}>
            {filter.map((data,index) =>
              <View style={styles.marginVertical}>
                <TouchableOpacity style={{width:320,height:120,backgroundColor:'#fff',borderRadius:5,elevation:5}} onPress={()=>this.props.navigation.navigate('DetailDestinasi',{id:data.id})}>
                  <Image source={{uri: 'http://indramayuapp.nusantech.co/uploads/pariwisata/' + data.image}} style={{position:'absolute',marginLeft:10,width:120,height:100,marginTop:10}}/>
                  <Text numberOfLines={2} style={{marginTop:10,width:180,fontSize:14,fontWeight:'bold',marginLeft:140,color:'#149BE5'}}>{data.KategoriDestinasi.kategoriName}</Text>    
                  <Text numberOfLines={2} style={{marginTop:10,width:180,fontSize:14,fontWeight:'bold',marginLeft:140,color:'#000000'}}>{data.destinasiName}</Text>
                  <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                     <Text numberOfLines={1} style={{width:150,fontSize:12}}>{data.OperationalTime}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          </ScrollView>
        )}


        <Modal style={{alignItems:'center'}} isVisible={this.state.modal}>
          <View style={styles.marginVertical}>
            <View style={styles.picker}>
              <Picker
                  selectedValue={this.state.TopikId}
                  onValueChange={(TopikId) => this.setState({TopikId})}>
                  <Picker.Item label='Pilih Filter' value={''} />
                  {datas.map((item, index) => {
                      return (<Picker.Item label={item.kategoriName} value={item.kategoriName} key={item.pendidikan}/>) 
                  })}
              </Picker>
            </View>
          </View>
          <View style={styles.marginVertical}>
            <TouchableOpacity style={styles.button} onPress={() => this.setState({ modal: false })}>
              <Text style={styles.textButton}>OK</Text>
            </TouchableOpacity>
          </View>
        </Modal>

        <Modal style={{alignItems:'center',backgroundColor:'#fff'}} isVisible={this.state.modal2}>
          <View style={{backgroundColor:'#fff',width:320,alignItems:'center'}}>
            <View style={styles.marginVertical}>
            </View>
            <View style={styles.marginVertical}>
               <View style={{backgroundColor:'#e0e0e0',flexDirection:'row',alignItems:'center',justifyContent:'center',width:Dimensions.get('window').width,height:50}}>
                  <TouchableOpacity style={{elevation:10,flex:1,justifyContent:'center',alignItems:'center'}} onPress={(()=>this.setState({modal:true}))}>
                    <Image style={{width:20,height:20}} source={require('./image/filter.png')}/>
                    <Text>Sort</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{elevation:10,flex:1,justifyContent:'center',alignItems:'center'}} onPress={(()=>this.setState({modal2:true}))}>
                    <Text>Filter</Text>
                  </TouchableOpacity>
                </View>
              <TouchableOpacity style={styles.button} onPress={() => this.setState({ modal2: false })}>
                <Text style={styles.textButton}>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

      </View>
    );
  }
}
        // <View style={{backgroundColor:'#e0e0e050',alignItems:'center',justifyContent:'center',width:Dimensions.get('window').width,height:50}}>
        //   <View style={{flexDirection:'row',backgroundColor:'#e0e0e050',alignItems:'center',justifyContent:'center',width:Dimensions.get('window').width,height:50}}>
        //   <TouchableOpacity style={{elevation:10,flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center'}} onPress={(()=>this.setState({modal:true}))}>
        //     <Image style={{width:20,height:20,marginRight:10}} source={require('./image/sort.png')}/>
        //     <Text>Sort</Text>
        //   </TouchableOpacity>
        //   <TouchableOpacity style={{elevation:10,flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center'}} onPress={(()=>this.setState({modal:true}))}>
        //     <Image style={{width:20,height:20,marginRight:10}} source={require('./image/filter.png')}/>
        //     <Text>Filter</Text>
        //   </TouchableOpacity>
        // </View>
        // </View>
