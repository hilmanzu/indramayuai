import React, {Component} from 'react';
import {Dimensions,Picker,ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../info/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import Communications from 'react-native-communications';
import store from 'react-native-simple-store';
const haversine = require('haversine')
import getDirections from 'react-native-google-maps-directions'
import HTML from 'react-native-render-html';
import renderIf from './renderIf'

export default class detail extends Component{

  constructor(props){
    super(props);
    this.state = {
      data :[],
      latitude:'',
      longitude:'',
      deslatitude:'',
      deslongitude:'',
      loading:true
      }
    }


   componentDidMount(){
    store.get('position').then((res) =>{
      const {params} = this.props.navigation.state;
        fetch( 'https://indramayuapp.nusantech.co/v1/pariwisatadestinasi/' + params.id )
        .then((response) => response.json())
        .then((responseJson) =>{
            this.setState({
              data        : responseJson , 
              loading     : false,
              deslatitude : parseFloat(res.initialPosition.latitude),
              deslongitude: parseFloat(res.initialPosition.longitude),
              latitude    : parseFloat(responseJson.Latitude),
              longitude   : parseFloat(responseJson.Longitude)
            })
        })
          .catch((error) =>{
            console.error(error);
        })
    })
  }

  handleGetDirections(e){
    const data = {
      source: {
        latitude: e.deslatitude,
        longitude: e.deslongitude
      },
      destination: {
        latitude: e.latitude,
        longitude: e.longitude
      },
      params: [
        {
          key: "travelmode",
          value: "driving"        // may be "walking", "bicycling" or "transit" as well
        },
        {
          key: "dir_action",
          value: "navigate"       // this instantly initializes navigation using the given travel mode 
        }
      ]
    }
    getDirections(data)
  }

  render() {
    const {data,longitude,latitude,deslongitude,deslatitude,loading} = this.state;
    var re = /(<[^>]+>|<[^>]>|<\/[^>]>)/g;

    if (loading === true) {
        return(
          <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
            <ActivityIndicator
              color={`rgba(0, 163, 192, 1)`}
              size={`large`}
              style={styles.s34e24b9b}
            />
            <Text style={{paddingTop:20}}> Menunggu Sebentar </Text>
          </View>
        )
      }

    return (
      <View style={styles.subcontainer}>
        <Image style={styles.ImageLogo} source={require('../info/back2.png')}/>
        <View style={styles.header}>
         <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>Destinasi Detail</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom:10}}>
          <Image source={{uri: 'http://indramayuapp.nusantech.co/uploads/pariwisata/' + data.image}} style={{width:Dimensions.get('window').width,height:200}}/>
          <View style={{flex:1,marginHorizontal:20}}>
              <Text numberOfLines={2} style={{width:300,fontWeight:'bold',marginTop:10,fontSize:14,color:'#007CBF'}}>
                {data.KategoriDestinasi.kategoriName}
              </Text>
              <Text numberOfLines={2} style={{width:300,fontWeight:'bold',marginTop:10,fontSize:20,color:'#000000'}}>
                {data.destinasiName}
              </Text>
              <Text numberOfLines={2} style={{width:300,fontWeight:'bold',marginTop:10,fontSize:14,color:'#000000'}}>
                {data.OperationalTime}
              </Text>
              <Text numberOfLines={1} style={{width:300,marginTop:10,fontSize:15,color:'#007CBF'}}>
                Detail
              </Text>
              <Text style={{marginTop:5,width:300}}>
                {data.destinasiDescription.replace(re,'')}
              </Text>
              <Text numberOfLines={1} style={{width:300,marginTop:10,fontSize:15,color:'#007CBF'}}>
                Lokasi
              </Text>
              <HTML style={{marginBottom:15}} html={data.destinasiAlamat} imagesMaxWidth={Dimensions.get('window').width}/>
          </View>
          <View style={{alignItems:'center',marginTop:20}}>
            <TouchableOpacity style={styles.button} onPress={this.handleGetDirections.bind(this,{latitude:latitude,longitude:longitude,deslatitude:deslatitude,deslongitude:deslongitude})}>
              <Text style={styles.textbutton}>Lihat Lokasi</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}