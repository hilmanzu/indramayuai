import React, {Component} from 'react';
import {Picker,ScrollView,TextInput,Image,TouchableOpacity,Platform, StyleSheet, Text, View} from 'react-native';
import styles from './src/component/style'
import list from './src/component/list.json'

const image = 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg'

export default class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      TopikId :''
      }
    }

  render() {
    return (
      <View style={styles.container}>

        <View style={styles.header}>
          <Text style={styles.textHeader}>Header</Text>
        </View>

        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.content}>

          <View style={styles.marginVertical}>
            <Text style={styles.Text}> Email </Text>
            <View style={styles.input}>
              <TextInput secureTextEntry={false} placeholder='Email' style={styles.inputText} onChangeText={(username)=> this.setState({username})}/>
            </View>
          </View>

          <View style={styles.marginVertical}>
            <Text style={styles.Text}> Picker </Text>
            <View style={styles.picker}>
              <Picker
                  selectedValue={this.state.TopikId}
                  onValueChange={(TopikId) => this.setState({TopikId})}>
                  <Picker.Item label='Pilih Data' value={null} />
                  {list.map((item, index) => {
                      return (<Picker.Item label={item.pendidikan} value={item.pendidikan} key={item.pendidikan}/>) 
                  })}
              </Picker>
            </View>
          </View>

          <View style={styles.marginVertical}>
            <TouchableOpacity style={styles.categoryButton}>
              <Image style={styles.categoryImage} source={{uri: image}}/>
              <Text numberOfLines={2} style={styles.categoryText}>Name</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.marginVertical}>
            <TouchableOpacity style={styles.listButton}>
              <Image style={styles.listImage} source={{uri: image}}/>
              <View>
                <Text numberOfLines={1} style={styles.listTextName}>Name</Text>
                <Text numberOfLines={2} style={styles.listTextDeskripsi}>Deskripsi</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.marginVertical}>
            <TouchableOpacity style={styles.button}>
              <Text style={styles.textButton}>Button</Text>
            </TouchableOpacity>
          </View>

        </ScrollView>

      </View>
    );
  }
}
