import React, {Component} from 'react';
import {ImageBackground,Picker,ScrollView,TextInput,Image,TouchableOpacity,Platform, StyleSheet, Text, View} from 'react-native';
import styles from '../style'

const image = 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg'

export default class kuliner extends Component {

  constructor(props){
    super(props);
    this.state = {
      data :[]
      }
    }


   componentDidMount(){
      fetch( 'https://indramayuapp.nusantech.co/v1/pariwisatakuliner' )
      .then((response) => response.json())
      .then((responseJson) =>{
          this.setState({
            data: responseJson.data , loading:false
          })
      })
        .catch((error) =>{
          console.error(error);
      })
  }

  render() {
    const {data} = this.state
    var re = /(<[^>]+>|<[^>]>|<\/[^>]>)/g;

    return (
      <View style={styles.container}>

        <View style={styles.header}>
        <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.textHeader}>Kuliner</Text>
        </View>

        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.content}>
          {data.map((data,index) =>
            <View style={styles.marginVertical}>
              <TouchableOpacity style={{width: 320,height: 120,elevation:1,borderRadius: 5}} onPress={()=>this.props.navigation.navigate('DetailKuliner',{id:data.id})}>
                <ImageBackground style={{width: 320,height: 120,position:'absolute'}} imageStyle={{ borderRadius: 10}} source={{uri: 'http://indramayuapp.nusantech.co/uploads/pariwisata/' + data.image}}>
                  <View style={{backgroundColor:'#00000050',width: 320,height: 120,borderRadius: 5}}>
                      <Text numberOfLines={1} style={{fontSize:18,fontWeight:'bold',color:'#fff',marginTop: 75,marginLeft:10,width:200}}>{data.kulinerName}</Text>
                  </View>
                </ImageBackground>
              </TouchableOpacity>
            </View>
          )}

        </ScrollView>

      </View>
    );
  }
}
