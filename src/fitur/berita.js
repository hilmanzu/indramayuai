import React, {Component} from 'react';
import {WebView,Dimensions,Picker,ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from './style/style'

export default class berita extends Component {

  render() {
    return (
    <View style={styles.subcontainer}>
      <View style={styles.header}>
          <TouchableOpacity onPress={() => {this.props.navigation.navigate('Menu')}} style={{position:'absolute',zIndex: 1}}>
              <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
            </TouchableOpacity>
          <Text style={styles.headertext}>Berita</Text>
        </View>
      <WebView
        source={{uri: 'https://indramayukab.go.id'}}
      />
  </View>
    );
  }
}