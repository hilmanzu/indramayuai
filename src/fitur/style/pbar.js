import React, {Component} from 'react';
import {Image,Dimensions,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from './style'
import store from 'react-native-simple-store';

class pbar extends Component{

  constructor(props){
    super(props);
    this.state={
      data: '',
      ditangani:'',
      selesai:''
    }  
  }

  componentDidMount(){
      fetch( 'https://indramayuapp.nusantech.co/v1/instansipengaduan' )
      .then((response) => response.json())
      .then((responseJson) =>{
        console.log(responseJson)
        store.get('id').then((res) =>{
          var filter = responseJson.data.filter((item) => item.Pengaduan.PelaporId == res)
          var data = filter
          var filter2 = data.filter((item) => item.Pengaduan.PenangananPengaduans.length == 0)
          var data2 = filter2
          var filter3 = data.filter((item) => item.Pengaduan.PenangananPengaduans.length !== 0)
          var data3 = filter3
          this.setState({
            data: data , loading:false,ditangani:data2.length,selesai:data3.length
          })
        })
      })
        .catch((error) =>{
          console.error(error);
      })
  }

  render() {  
    const {data} = this.state
    return (
          <View style={styles.pbar}>
            <View style={styles.pbard}>
              <View style={styles.textpbar2}>
                <Text style={styles.texttotal}>
                  {this.state.ditangani}
                </Text>
                <Text style={styles.texttotal2}>
                  Pengaduan
                </Text>
                <Text style={styles.texttotal3}>
                  On Progress
                </Text>
              </View>
              <View style={styles.textpbar}>
                <Text style={styles.texttotal}>
                  {this.state.selesai}
                </Text>
                <Text style={styles.texttotal2}>
                  Pengaduan
                </Text>
                <Text style={styles.texttotal3}>
                  Ditangani
                </Text>
              </View>
              <View style={styles.textpbar2}>
                <Text style={styles.texttotal}>
                  {data.length}
                </Text>
                <Text style={styles.texttotal2}>
                  Total
                </Text>
                <Text style={styles.texttotal3}>
                  Pengaduan
                </Text>
              </View>
            </View>
          </View>
    );
  }
}

export default pbar