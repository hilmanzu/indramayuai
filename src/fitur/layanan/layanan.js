import React, {Component} from 'react';
import {ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import data from './data/layanan.json'
import Communications from 'react-native-communications';

export default class layanan extends Component{
  constructor(props){
    super(props);
    this.state = {
      email   :'',
      password:'',
      ktp     :'',
      first   :'',
      last    :'',
      hp      :'',
      gender  :'',
      showAlert: false,
      color   :'rgba(247, 247, 247, 1)'
      }
    }

  render() {
    console.log(data)
    return (
      <View style={styles.subcontainer}>
        <View style={styles.header}>
          <View style={{position:'absolute',zIndex: 1}}>
            <TouchableOpacity onPress={() => {this.props.navigation.openDrawer()}}>
              <Image source={require('../image/menu.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
            </TouchableOpacity>
          </View>
          <Text style={styles.headertext}>Layanan Darurat</Text>
        </View>
        <View style={styles.const}>
        <ScrollView showsVerticalScrollIndicator={false}>

        {data.map((data)=>
          <View style={{width:320,height:144,backgroundColor:data.warna,marginTop:20,borderRadius:10}}>
            <Text style={styles.textbl1}>{data.first}</Text>
            <Text style={styles.textbl2}>{data.last}</Text>
            <Image source={{uri : data.image}} style={styles.imagebl}/>
            <TouchableOpacity style={styles.boxlayanan2} onPress={() => Communications.phonecall(data.number, true)}>
              <View style={{flexDirection:'row',alignItems:'center'}}>
                <Image source={require('../image/phone.png')} style={styles.imagebl2}/>
                <Text style={{marginLeft:5,fontSize:15,color:'#FF1919'}}>Call</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.boxlayanan3} onPress={() => this.props.navigation.navigate(data.data,{list:data.data})}>
              <View style={{flexDirection:'row',alignItems:'center'}}>
                <Image source={require('../image/menu.png')} style={styles.imagebl3}/>
                <Text style={{marginLeft:5,fontSize:15}}>List</Text>
              </View>
            </TouchableOpacity>
          </View>
          )
        }

        </ScrollView>
        </View>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          message="Harap Menunggu"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
      </View>
    );
  }

  daftar=()=>{
    if (this.state.ktp == ''){
      alert('Isi No KTP')
    }else if (this.state.first == ''){
      alert('Isi Nama Depan')
    }else if (this.state.last == ''){
      alert('Isi Nama Belakang')
    }else if (this.state.email == ''){
      alert('Isi Email')
    }else if (this.state.password == ''){
      alert('Isi password anda')
    }else if (this.state.hp == ''){
      alert('Isi No Handphone')
    }else if (this.state.gender == ''){
      alert('Isi Jenis Kelamin')
    }else {
      this.setState({showAlert: true})
      fetch('https://indramayuapp.nusantech.co/v1/auth/signup', {
        method: 'POST',
        headers: {'Content-Type': 'application/json','Accept':'application/json' 
        },
        body: JSON.stringify({
            firstName : this.state.first,
            lastName  : this.state.last,
            email     : this.state.email,
            password  : this.state.password,
            phone     : this.state.hp,
            noKtp     : this.state.ktp,
            gender    : this.state.gender
        })
      })
      .then((response)=> response.json())
      .then((responseJson) =>{    
        if (responseJson.id){
          Alert.alert('Selamat Datang', 'Pendaftaran Berhasil')
          this.setState({showAlert: false})
          this.props.navigation.navigate('Login')
        }else{
          Alert.alert('Tidak Dapat Mendaftar',responseJson[0].message)
          this.setState({showAlert: false})
        }
      })
      .catch((error) =>{
          Alert.alert('Tidak dapat daftar','Periksa Koneksi Anda')
          this.setState({button:false})
      })
    }
  }

}