import React, {Component} from 'react';
import {ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import Communications from 'react-native-communications';
import store from 'react-native-simple-store';
const haversine = require('haversine')
import getDirections from 'react-native-google-maps-directions'

export default class pemadam extends Component{
  constructor(props){
    super(props);
    this.state = {
      latitude:'',
      longitude:'',
      showAlert: false,
      loading:true,
      sort :''
      }
    }

  componentWillMount(){
      store.get('position').then((res) =>{
        fetch('https://indramayuapp.nusantech.co/v1/pemadam', {
          method: 'GET',
          headers: {  
                'Accept':'application/json',
                'Content-Type':'application/json'}
              })
        .then((response) => response.json())
        .then((responseJson) =>{
            var respon = responseJson.data
            var item = []
            console.log(respon)
            respon.forEach((data)=>{
              let datas = item
              datas.push({
                jarak       : haversine({latitude:res.initialPosition.latitude,longitude:res.initialPosition.longitude},{latitude:data.Latitude,longitude:data.Longitude}, {unit: 'kilometer'}),
                pemadamName  : data.pemadamName,
                image       : 'http://indramayuapp.nusantech.co/uploads/pemadam/' + data.image,
                pemadamTelp  : data.pemadamTelp,
                pemadamAlamat: data.pemadamAlamat,
                pemadamInformasi:data.pemadamInformasi,
                latitude : parseFloat(data.Latitude),
                longitude : parseFloat(data.Longitude),
                deslatitude : parseFloat(res.initialPosition.latitude),
                deslongitude : parseFloat(res.initialPosition.longitude)
              })
            })
          this.setState({
            sort : item.sort(function(a, b){return a.jarak - b.jarak}),loading:false
          })
        })
      })
  }

  handleGetDirections(e){
    const data = {
      source: {
        latitude: e.deslatitude,
        longitude: e.deslongitude
      },
      destination: {
        latitude: e.latitude,
        longitude: e.longitude
      },
      params: [
        {
          key: "travelmode",
          value: "driving"        // may be "walking", "bicycling" or "transit" as well
        },
        {
          key: "dir_action",
          value: "navigate"       // this instantly initializes navigation using the given travel mode 
        }
      ]
    }
    getDirections(data)
  }

  render() {
    const { sort,loading } = this.state
    var re = /(<[^>]+>|<[^>]>|<\/[^>]>)/g;


    if (loading === true) {
        return(
          <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
            <ActivityIndicator
              color={`rgba(0, 163, 192, 1)`}
              size={`large`}
              style={styles.s34e24b9b}
            />
            <Text style={{paddingTop:20}}> Menunggu Sebentar </Text>
          </View>
        )
      }

    return (
      <View style={styles.subcontainer}>
        <View style={styles.header}>
          <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>Kepemadaman Terdekat</Text>
        </View>
        <View style={styles.const}>
        <ScrollView showsVerticalScrollIndicator={false}>

        {sort.map((data)=>
          <View style={{marginVertical:10,marginHorizontal:2}}>
            <TouchableOpacity style={{width:320,height:120,backgroundColor:'#fff',borderRadius:5,elevation:5}} onPress={()=>this.props.navigation.navigate('DetailLayanan',{latitude:data.latitude,longitude:data.longitude,deslatitude:data.deslatitude,deslongitude:data.deslongitude,image:data.image,deskripsi:data.pemadamInformasi,nama:data.pemadamName,alamat:data.pemadamAlamat})}>
              <Image source={{uri: data.image}} style={{position:'absolute',marginLeft:10,width:120,height:100,marginTop:10}}/>
              <Text numberOfLines={2} style={{marginTop:10,width:180,fontSize:14,fontWeight:'bold',marginLeft:140,color:'#149BE5'}}>{data.pemadamName}</Text>
              <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                <Image source={require('../image/pin.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                <Text numberOfLines={1} style={{width:150,fontSize:12,marginLeft:5}}>{data.pemadamAlamat.replace(re,'')}</Text>
              </View>
              <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                <Image source={require('../image/phone.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                <Text style={{fontSize:12,marginLeft:5}}>{data.pemadamTelp}</Text>
              </View>
              <View style={{flexDirection:'row',marginLeft:200,alignItems:'center',marginTop:90,position:'absolute'}}>
                <Text numberOfLines={1} style={{fontSize:14,marginLeft:5,width:100,fontWeight:'bold',color:'#149BE5',textAlign:'right'}}>{data.jarak.toFixed(1)} KM</Text>
              </View>
            </TouchableOpacity>
          </View>
          )
        }

        </ScrollView>
        </View>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          message="Harap Menunggu"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
      </View>
    );
  }
}