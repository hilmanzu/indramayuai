import React, {Component} from 'react';
import {Dimensions,Picker,ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import Communications from 'react-native-communications';
import store from 'react-native-simple-store';
const haversine = require('haversine')
import getDirections from 'react-native-google-maps-directions'
import HTML from 'react-native-render-html';

export default class detail extends Component{

  handleGetDirections(e){
    const data = {
      source: {
        latitude: e.deslatitude,
        longitude: e.deslongitude
      },
      destination: {
        latitude: e.latitude,
        longitude: e.longitude
      },
      params: [
        {
          key: "travelmode",
          value: "driving"        // may be "walking", "bicycling" or "transit" as well
        },
        {
          key: "dir_action",
          value: "navigate"       // this instantly initializes navigation using the given travel mode 
        }
      ]
    }
    getDirections(data)
  }

  render() {
    const {params} = this.props.navigation.state;
    var re = /(<[^>]+>|<[^>]>|<\/[^>]>)/g;

    return (
      <View style={styles.subcontainer}>
        <Image style={styles.ImageLogo} source={require('../image/back2.png')}/>
        <View style={styles.header}>
          <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>Detail Peribadatan</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom:10}} contentContainerStyle={{alignItems:'center'}}>
          <Image source={{uri: params.image}} style={{width:Dimensions.get('window').width,height:200}}/>

          <View style={{flex:1}}>
              <Text numberOfLines={2} style={{width:300,fontWeight:'bold',marginTop:10,fontSize:20,color:'#007CBF'}}>
                {params.nama}
              </Text>
              <View style={{flexDirection:'row',alignItems:'center'}}>
                <Image source={require('../image/pin.png')} style={{width:15,height:15}}/>
                <Text numberOfLines={5} style={{marginTop:5,marginLeft:10,width:250}}>
                  {params.alamat.replace(re,'')}
                </Text>
              </View>
              <Text numberOfLines={1} style={{width:300,marginTop:10,fontSize:15,color:'#007CBF'}}>
                Deskripsi
              </Text>
              <Text style={{marginTop:5,width:300}}>
                {params.deskripsi.replace(re,'')}
              </Text>
          </View>

          <View style={{alignItems:'center',marginTop:20}}>
            <TouchableOpacity style={styles.button} onPress={this.handleGetDirections.bind(this,{latitude:params.latitude,longitude:params.longitude,deslatitude:params.deslatitude,deslongitude:params.deslongitude})}>
              <Text style={styles.textbutton}>Lihat Lokasi</Text>
            </TouchableOpacity>
          </View>
        
        </ScrollView>
      </View>
    );
  }
}