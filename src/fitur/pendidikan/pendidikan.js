import React, {Component} from 'react';
import {ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert,Picker} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import Communications from 'react-native-communications';
import store from 'react-native-simple-store';
const haversine = require('haversine')
import getDirections from 'react-native-google-maps-directions'
import SearchInput, { createFilter } from 'react-native-search-filter';
import renderIf from './renderIf'

const KEYS = ['akademikName'];

export default class akademik extends Component{
  constructor(props){
    super(props);
    this.state = {
      latitude:'',
      longitude:'',
      showAlert: false,
      loading:true,
      sort :[],
      data2 :[],
      searchTerm: '',
      ccategory:'',
      data:'',
      lokasi:''
      }
    }

  componentWillMount(){
      const { params } = this.props.navigation.state;
      store.get('position').then((res) =>{
        fetch('https://indramayuapp.nusantech.co/v1/akademik', {
          method: 'GET',
          headers: {  
                'Accept':'application/json',
                'Content-Type':'application/json'}
              })
        .then((response) => response.json())
        .then((responseJson) =>{
            var filter = responseJson.data.filter((item) => item.JenisPendidikanId === params.akademik)
            var item = []
              filter.forEach((data)=>{
                let datas = item
                datas.push({
                  jarak       : haversine({latitude:res.initialPosition.latitude,longitude:res.initialPosition.longitude},{latitude:data.Latitude,longitude:data.Longitude}, {unit: 'kilometer'}),
                  akademikName  : data.akademikName,
                  image       : 'http://indramayuapp.nusantech.co/uploads/pendidikan/' + data.image,
                  akademikTelp  : data.akademikTelp,
                  akademikAlamat: data.akademikAlamat,
                  akademikInformasi:data.akademikInformasi,
                  JenisakademikId: data.JenisPendidikanId,
                  latitude : parseFloat(data.Latitude),
                  longitude : parseFloat(data.Longitude),
                  deslatitude : res.initialPosition.latitude,
                  deslongitude : res.initialPosition.longitude
                })
              })
            this.setState({
              sort : item.sort(function(a, b){return a.jarak - b.jarak}),
              data2: responseJson.data,
              lokasi : res
            })

            fetch( 'https://indramayuapp.nusantech.co/v1/JenisPendidikan')
              .then((response) => response.json())
              .then((responseJson) =>{
                  this.setState({
                    data: responseJson.data , loading:false
                  })
              })
                .catch((error) =>{
                  console.error(error);
            })

        })
      })
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }

  handleGetDirections(e){
    const data = {
      source: {
        latitude: e.deslatitude,
        longitude: e.deslongitude
      },
      destination: {
        latitude: e.latitude,
        longitude: e.longitude
      },
      params: [
        {
          key: "travelmode",
          value: "driving"        // may be "walking", "bicycling" or "transit" as well
        },
        {
          key: "dir_action",
          value: "navigate"       // this instantly initializes navigation using the given travel mode 
        }
      ]
    }
    getDirections(data)
  }

  render() {
    const { data,sort,loading,ccategory,data2,lokasi } = this.state
    var re = /(<[^>]+>|<[^>]>|<\/[^>]>)/g;
    const filtered = sort.filter(createFilter(this.state.searchTerm, KEYS))

    var filter2 = data2.filter((item) => item.JenisPendidikanId === ccategory)
    var item2 = []
      filter2.forEach((data)=>{
        let datas = item2
        datas.push({
          jarak       : haversine({latitude:lokasi.initialPosition.latitude,longitude:lokasi.initialPosition.longitude},{latitude:data.Latitude,longitude:data.Longitude}, {unit: 'kilometer'}),
          akademikName  : data.akademikName,
          image       : 'http://indramayuapp.nusantech.co/uploads/pendidikan/' + data.image,
          akademikTelp  : data.akademikTelp,
          akademikAlamat: data.akademikAlamat,
          akademikInformasi:data.akademikInformasi,
          JenisakademikId: data.JenisPendidikanId,
          latitude : parseFloat(data.Latitude),
          longitude : parseFloat(data.Longitude),
          deslatitude : lokasi.initialPosition.latitude,
          deslongitude : lokasi.initialPosition.longitude
        })
      })
    var file = item2.sort(function(a, b){return a.jarak - b.jarak})
    const filtered2 = file.filter(createFilter(this.state.searchTerm, KEYS))


    if (loading === true) {
        return(
          <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
            <ActivityIndicator
              color={`rgba(0, 163, 192, 1)`}
              size={`large`}
              style={styles.s34e24b9b}
            />
            <Text style={{paddingTop:20}}> Menunggu Sebentar </Text>
          </View>
        )
      }

    return (
      <View style={styles.subcontainer}>
        <View style={styles.header}>
        <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
        </TouchableOpacity>
          <Text style={styles.headertext}>Daftar Lokasi Pendidikan</Text>
        </View>
        <View style={styles.const}>
        <Image source={require('../image/back2.png')} style={styles.ImageLogo2}/>
        {renderIf(ccategory !== '' ? true : false)(
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center',marginTop:10}}>
            <View style={{backgroundColor:'#fff',marginTop:20,width:320,height:195,alignItems:'center',elevation:5,borderRadius:5}}>
                <Text style={{fontWeight:'bold',width:100,marginTop:20,marginRight:170,marginBottom:10}}>
                  Kategori
                </Text>
                <View style={styles.itempekerjaan}>
                  <Picker
                    selectedValue={this.state.ccategory}
                    onValueChange={(ccategory) => this.setState({ccategory})}>
                    <Picker.Item label='Pilih Kategori' value={''} />
                    {data.map((item, index) => {
                    return (<Picker.Item label={item.jenisName} value={item.id} key={item.id}/>) 
                    })}
                  </Picker>
                </View>

                <Text style={{fontWeight:'bold',width:100,marginTop:10,marginRight:170}}>
                  Search
                </Text>
                <View style={{backgroundColor:'#F2FBFF',borderColor: '#149BE5',borderRadius:10,borderWidth: 1,height:40,width:280,marginTop:20,marginTop:15}}>
                  <SearchInput 
                    onChangeText={(term) => { this.searchUpdated(term) }} 
                    style={styles.searchInput}
                    placeholder=" "
                  />
                </View>
            </View>
          {filtered2.map((data)=>
            <View style={{marginVertical:10,marginHorizontal:2}}>
              <TouchableOpacity style={{width:320,height:120,backgroundColor:'#fff',borderRadius:5,elevation:5}} onPress={()=>this.props.navigation.navigate('DetailPendidikan',{latitude:data.latitude,longitude:data.longitude,deslatitude:data.deslatitude,deslongitude:data.deslongitude,image:data.image,deskripsi:data.akademikInformasi,nama:data.akademikName,telp:data.akademikTelp,alamat:data.akademikAlamat})}>
                <Image source={{uri: data.image}} style={{position:'absolute',marginLeft:10,width:120,height:100,marginTop:10}}/>
                <Text numberOfLines={2} style={{marginTop:10,width:180,fontSize:14,fontWeight:'bold',marginLeft:140,color:'#149BE5'}}>{data.akademikName}</Text>
                <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                  <Image source={require('../image/pin.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                  <Text numberOfLines={1} style={{width:150,fontSize:12,marginLeft:5}}>{data.akademikAlamat.replace(re,'')}</Text>
                </View>
                <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                  <Image source={require('../image/phone.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                  <Text style={{fontSize:12,marginLeft:5}}>{data.akademikTelp}</Text>
                </View>
              </TouchableOpacity>
            </View>
            )
          }
          </ScrollView>
        )}

        {renderIf(ccategory == '' ? true : false)(
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center',marginTop:10}}>
            <View style={{backgroundColor:'#fff',marginTop:20,width:320,height:195,alignItems:'center',elevation:5,borderRadius:5}}>
                <Text style={{fontWeight:'bold',width:100,marginTop:20,marginRight:170,marginBottom:10}}>
                  Kategori
                </Text>
                <View style={styles.itempekerjaan}>
                  <Picker
                    selectedValue={this.state.ccategory}
                    onValueChange={(ccategory) => this.setState({ccategory})}>
                    <Picker.Item label='Pilih Kategori' value={''} />
                    {data.map((item, index) => {
                    return (<Picker.Item label={item.jenisName} value={item.id} key={item.id}/>) 
                    })}
                  </Picker>
                </View>

                <Text style={{fontWeight:'bold',width:100,marginTop:10,marginRight:170}}>
                  Search
                </Text>
                <View style={{backgroundColor:'#F2FBFF',borderColor: '#149BE5',borderRadius:10,borderWidth: 1,height:40,width:280,marginTop:20,marginTop:15}}>
                  <SearchInput 
                    onChangeText={(term) => { this.searchUpdated(term) }} 
                    style={styles.searchInput}
                    placeholder=" "
                  />
                </View>
            </View>
          {filtered.map((data)=>
            <View style={{marginVertical:10,marginHorizontal:2}}>
              <TouchableOpacity style={{width:320,height:120,backgroundColor:'#fff',borderRadius:5,elevation:5}} onPress={()=>this.props.navigation.navigate('DetailPendidikan',{latitude:data.latitude,longitude:data.longitude,deslatitude:data.deslatitude,deslongitude:data.deslongitude,image:data.image,deskripsi:data.akademikInformasi,nama:data.akademikName,telp:data.akademikTelp,alamat:data.akademikAlamat})}>
                <Image source={{uri: data.image}} style={{position:'absolute',marginLeft:10,width:120,height:100,marginTop:10}}/>
                <Text numberOfLines={2} style={{marginTop:10,width:180,fontSize:14,fontWeight:'bold',marginLeft:140,color:'#149BE5'}}>{data.akademikName}</Text>
                <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                  <Image source={require('../image/pin.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                  <Text numberOfLines={1} style={{width:150,fontSize:12,marginLeft:5}}>{data.akademikAlamat.replace(re,'')}</Text>
                </View>
                <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                  <Image source={require('../image/phone.png')} style={{width:15,height:15,tintColor:'#149BE5'}}/>
                  <Text style={{fontSize:12,marginLeft:5}}>{data.akademikTelp}</Text>
                </View>
              </TouchableOpacity>
            </View>
            )
          }
          </ScrollView>
        )}
        </View>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          message="Harap Menunggu"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
      </View>
    );
  }
}