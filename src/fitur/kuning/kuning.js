import React, {Component} from 'react';
import {Dimensions,Picker,ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import HTML from 'react-native-render-html';
import kuning from './kuning.json'

export default class detail extends Component{

  render() {
    const {params} = this.props.navigation.state;

    return (
      <View style={styles.subcontainer}>
        <View style={styles.header}>
        <TouchableOpacity onPress={() => {this.props.navigation.navigate('Menu')}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>Pembutan Kartu Kuning</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} style={{width:Dimensions.get('window').width,marginBottom:10}}>
          <Image style={styles.ImageLogo} source={require('../image/back2.png')}/>
          <View style={{backgroundColor:'#fff',elevation:5,margin:10}}>
            <Text numberOfLines={3} style={{width:300,fontWeight:'bold',marginHorizontal:20,marginVertical:20,color:'#149BE5'}}>
                Persyaratan pembuatan Kartu Kuning
            </Text>
            <View style={{marginHorizontal:10}}>
              <HTML style={{marginHorizontal:20}} html={kuning.data.jenisName} imagesMaxWidth={Dimensions.get('window').width}/>
            </View>
          </View>
          <View style={{alignItems:'center',marginVertical:10,marginBottom:20}}>
            <TouchableOpacity style={{borderRadius:30,backgroundColor:'#149BE5',height:52,width:300,justifyContent:'center',elevation:5}} onPress={()=>this.props.navigation.navigate('Sub')}>
              <Text style={{color:'#FFFFFF',fontSize: 15,textAlign:'center',fontWeight:'bold'}}>Web Kartu Kuning</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}