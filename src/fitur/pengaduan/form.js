import React, {Component} from 'react';
import {ActivityIndicator,Picker,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import ImagePicker from 'react-native-image-picker';
import store from 'react-native-simple-store';
import Modal from "react-native-modal";

export default class form extends Component{
  constructor(props){
    super(props);
    this.state = {
      TopikId             :'Pilih Topik',
      TopikId2            :'',
      InstansiId2         :'',
      pengaduanTitle      :'',
      pengaduanDescription:'',
      pengaduanPhone      :'',
      pengaduanLocation   :'',
      avatarSource        :'http://icons.iconarchive.com/icons/graphicloads/100-flat-2/256/arrow-upload-icon.png',
      data                :[],
      data2               :[],
      Latitude            :'',
      Longitude           :'',
      fileName            :'',
      showAlert           : false,
      loading             :true,
      InstansiId          :'',
      kecamatan           :'Pilih Kecamatan',
      kecamatanid         :'',
      newValue: '',
      height: 40
      }
    }

  choosePicture = () =>{
      var ImagePicker = require('react-native-image-picker');
      var options = {
          title: 'Pilih Gambar',
          storageOptions: {
            skipBackup: true,
            path: 'images'
          }
      };
      ImagePicker.showImagePicker(options, (response) =>{
          console.log('Response = ', response);
          if (response.didCancel) {
            console.log('User cancelled image picker');
          }
          else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }
          else {
            console.log(response.fileName);
            this.setState({
              srcImg: { uri: response.uri },
              avatarSource: response.uri,
              fileName: response.fileName
            });
          }
      });
  };

  componentDidMount(){
    store.get('position').then((res) =>{
      fetch( 'https://indramayuapp.nusantech.co/v1/topik' )
      .then((response) => response.json())
      .then((responseJson) =>{
          this.setState({
            data : responseJson.data,Latitude:res.initialPosition.latitude,Longitude:res.initialPosition.longitude,loading:false
          })
          
          fetch( 'https://indramayuapp.nusantech.co/v1/masterkecamatan' )
          .then((response) => response.json())
          .then((responseJson) =>{
              this.setState({
                data2 : responseJson.data,loading:false
              })
          })
            .catch((error) =>{
              console.error(error);
          })
      })
        .catch((error) =>{
          console.error(error);
      })
    })
  }

  _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });

  _toggleModal2 = () =>
    this.setState({ isModalVisible2: !this.state.isModalVisible2 });

  updateSize = (height) => {
    this.setState({
      height
    });
  }

  render() {
    const {data,data2,Longitude,Latitude,loading,InstansiId,newValue, height} = this.state
    var sort = data2.sort((a, b) => {return a.kecamatanName.localeCompare(b.kecamatanName);})
    let newStyle = {
      height,
      backgroundColor:'#F2FBFF',
      borderColor: '#149BE5',
      borderRadius:10,
      borderWidth: 1,
      width:280,
      marginHorizontal:20,
      marginTop:15
    }

    if (loading === true) {
        return(
          <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
            <ActivityIndicator
              color={`rgba(0, 163, 192, 1)`}
              size={`large`}
              style={styles.s34e24b9b}
            />
            <Text style={{paddingTop:20}}> Menunggu Sebentar </Text>
          </View>
        )
      }
    
    return (
      <View style={styles.subcontainer}>
        <View style={styles.header}>
        <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>Form Pengaduan</Text>
        </View>
        <View style={styles.const}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={styles.textfirst}>
            Topik
          </Text>
          <TouchableOpacity style={styles.item} onPress={()=>this.setState({isModalVisible: !this.state.isModalVisible})}>
            <Text style={{marginLeft:5}}>{this.state.TopikId}</Text>
          </TouchableOpacity>
          <Text style={styles.textfirst}>
            Instansi
          </Text>
          <View style={styles.item}>
              <Text style={{marginLeft:5}}>{this.state.InstansiId}</Text>
          </View>
          <Text style={styles.textpassword}>
            Judul Pengaduan
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(pengaduanTitle)=> this.setState({pengaduanTitle})}/>
          </View>
          <Text style={styles.textpassword}>
            Pesan
          </Text>
          <View >
              <TextInput 
                multiline={true} 
                editable={true}
                style={[newStyle]}
                onContentSizeChange={(e) => this.updateSize(e.nativeEvent.contentSize.height)}
                onChangeText={(pengaduanDescription)=> this.setState({pengaduanDescription})}/>
          </View>
          <Text style={styles.textpassword}>
            Telepon yang dapat dihubungi
          </Text>
          <View style={styles.textinput}>
              <TextInput keyboardType='numeric' onChangeText={(pengaduanPhone)=> this.setState({pengaduanPhone})}/>
          </View>
          <Text style={styles.textfirst}>
            Kecamatan
          </Text>
          <TouchableOpacity style={styles.item} onPress={()=>this.setState({isModalVisible2: !this.state.isModalVisible2})}>
            <Text style={{marginLeft:5}}>{this.state.kecamatan}</Text>
          </TouchableOpacity>
          <Text style={styles.textpassword}>
            Lokasi
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(pengaduanLocation)=> this.setState({pengaduanLocation})}/>
          </View>
          <Text style={styles.textpassword}>
            Upload Gambar
          </Text>

          <TouchableOpacity onPress={this.choosePicture} style={{elevation:5,width:90,height:90,marginLeft:20,marginTop:10}}>
            <Image source={{uri:this.state.avatarSource}} style={{width:91,height:91,position:'absolute'}} />
          </TouchableOpacity>

          <View style={{alignItems:'center',marginVertical:20}}>
            <TouchableOpacity style={styles.button} disabled={this.state.button} onPress={this.daftar}>
              <Text style={styles.textbutton}>Buat Pengaduan</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        </View>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          message="Harap Menunggu"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <Modal isVisible={this.state.isModalVisible}>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1,justifyContent:'center'}}>
            {data.map((data) =>
              <TouchableOpacity  onPress={()=>this.setState({isModalVisible: !this.state.isModalVisible,TopikId:data.topikName,InstansiId:data.Instansi.instansiName,TopikId2:data.id,InstansiId2:data.Instansi.id})} style={{justifyContent:'center'}}>
                <View style={{ justifyContent:'center',backgroundColor:'#F2FBFF',height:40,width:280,marginHorizontal:20}}>
                  <Text style={{marginLeft:20}}>{data.topikName}</Text>
                </View>
              </TouchableOpacity>    
            )}
          </ScrollView>
        </Modal>
        <Modal isVisible={this.state.isModalVisible2}>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1,justifyContent:'center'}}>
            {sort.map((data) =>
              <TouchableOpacity  onPress={()=>this.setState({isModalVisible2: !this.state.isModalVisible2,kecamatan:data.kecamatanName,kecamatanid:data.id})} style={{justifyContent:'center'}}>
                <View style={{ justifyContent:'center',backgroundColor:'#F2FBFF',height:40,width:280,marginHorizontal:20}}>
                  <Text style={{marginLeft:20}}>{data.kecamatanName}</Text>
                </View>
              </TouchableOpacity>    
            )}
          </ScrollView>
        </Modal>
      </View>
    );
  }

  daftar=()=>{
    if (this.state.TopikId == 'Pilih Topik'){
      alert('Pilih Topik')
    }else if (this.state.pengaduanTitle == ''){
      alert('Isi Judul')
    }else if (this.state.pengaduanDescription == ''){
      alert('Isi Pesan')
    }else if (this.state.pengaduanPhone == ''){
      alert('Isi No Hp')
    }else if (this.state.pengaduanLocation == ''){
      alert('Isi Nama Lokasi')
    }else if (this.state.fileName == ''){
      alert('Pilih Gambar')
    }else{
      this.setState({showAlert: true})
      let formData = new FormData()
      formData.append('TopikId', this.state.TopikId2)
      formData.append('pengaduanTitle', this.state.pengaduanTitle)
      formData.append('pengaduanDescription', this.state.pengaduanDescription)
      formData.append('pengaduanPhone', this.state.pengaduanPhone)
      formData.append('pengaduanLocation', this.state.pengaduanLocation)
      formData.append('MasterKecamatanId', this.state.kecamatanid)
      formData.append('Latitude', this.state.Latitude)
      formData.append('Longitude', this.state.Longitude)
      formData.append('InstansiId', this.state.InstansiId2)
      formData.append('img', {
                              uri : this.state.avatarSource,
                              type: "image/jpeg",
                              name: this.state.fileName
                      })

      store.get('token').then((res) =>{
      fetch('https://indramayuapp.nusantech.co/v1/pengaduan', {
        method: 'POST',
        headers: {  
              'Accept':'application/json',
              'Content-Type':'multipart/form-data',
              'Authorization': res },
        body: formData
      })
      .then((response)=> response.json())
      .then((responseJson) =>{    
        console.log(responseJson)
        if (responseJson.message === "created"){
          Alert.alert('Berhasil', 'Pengaduan anda akan kami proses')
          this.setState({showAlert: false})
          this.props.navigation.navigate('Pengaduan')
        }else{
          Alert.alert('Error',responseJson[0].message)
          this.setState({showAlert: false})
        }
      })
      .catch((error) =>{
          console.log(error)
          Alert.alert('Tidak dapat buat pengaduan','Periksa Koneksi Anda atau ulangi kembali')
          this.setState({showAlert: false})
          this.setState({button:false})
      })
      })
    }
  }
}