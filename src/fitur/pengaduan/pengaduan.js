import React, {Component} from 'react';
import {ScrollView,ActivityIndicator,Image,Dimensions,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../style/style'
import Pbar from '../style/pbar'
import store from 'react-native-simple-store';
import AwesomeAlert from 'react-native-awesome-alerts';
import renderIf from './renderIf'

export default class Pengaduan extends Component{

  constructor(props){
    super(props);
    this.state={
      data: '',
      loading:true,
      showAlert:false
    }  
  }

  componentDidMount(){
    store.get('id').then((res) =>{
      fetch( 'https://indramayuapp.nusantech.co/v1/instansipengaduan' )
      .then((response) => response.json())
      .then((responseJson) =>{
          var filter = responseJson.data.filter((item) => item.Pengaduan.PelaporId == res)
          var data = filter
          this.setState({
            data: data , loading:false
          })
      })
        .catch((error) =>{
          console.error(error);
      }),this.timer = setTimeout(() => this.componentDidMount(), 5000)
    })
  }

  render() {  
    const {data,loading,showAlert} = this.state;

    if (loading === true) {
        return(
          <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
            <ActivityIndicator
              color={`rgba(0, 163, 192, 1)`}
              size={`large`}
              style={styles.s34e24b9b}
            />
            <Text style={{paddingTop:20}}> Menunggu Sebentar </Text>
          </View>
        )
      }

    return (
      <ImageBackground style={styles.ImageBackground} source={require('../image/back.png')}>
        <View style={{flex: 1,alignItems:'center'}}>
          <Image style={styles.ImageLogo} source={require('../image/back2.png')}/>
          <View style={styles.header}>
            <View style={{position:'absolute',zIndex: 1}}>
              <TouchableOpacity onPress={() => {this.props.navigation.openDrawer()}}>
                <Image source={require('../image/menu.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
              </TouchableOpacity>
            </View>
            <Text style={styles.headertext}>Pengaduan</Text>
          </View>
          <View>
            <Pbar/>
            <View style={{paddingTop:20,paddingBottom:20}}>
              <Text style={{fontSize:20,textAlign:'left'}}>Pengaduan Saya</Text>
            </View>
          </View>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center'}}>
              <View style={styles.introcontainer}>
                {data.map((data) =>
                  <View style={{borderRadius:10,marginTop:10,height:100,width:320,flexDirection:'row',elevation:5,backgroundColor:'#fff'}}>
                    <View style={{width:7,backgroundColor: data.Pengaduan.PenangananPengaduans[0].Status.statusName === 'Selesai' ? '#149BE5' : '#48F20D'}}/>
                    <View>
                      <Image source={{uri:'http://indramayuapp.nusantech.co/uploads/pengaduan/' + data.Pengaduan.image}} style={{width:60,height:60,margin:20}}/>
                    </View>
                    <View>
                      <Text numberOfLines={1} style={{width:200,marginTop:10,fontWeight:'bold'}}>
                        {data.Pengaduan.pengaduanTitle}
                      </Text>
                      <Text numberOfLines={1} style={{marginTop:5,width:200,}}>
                        Instansi : {data.Pengaduan.Topik.Instansi.instansiName}
                      </Text>
                      <Text numberOfLines={1} style={{marginTop:5,width:200,}}>
                        Status : {data.Pengaduan.PenangananPengaduans[0].Status.statusName}
                      </Text>
                    </View>
                  </View>
                )}
                <View style={{alignItems:'center',marginTop:20,marginBottom:20}}>
                  <TouchableOpacity style={styles.button} onPress={this.pengaduan}>
                    <Text style={styles.textbutton}>Buat Pengaduan</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
            <AwesomeAlert
              show={showAlert}
              message="Laporan anda sebelumnya sedang di proses, lakukan pelaporan setelah laporan anda sebelumnya sudah di verifikasi"
              closeOnTouchOutside={true}
              closeOnHardwareBackPress={false}
              showConfirmButton={true}
              confirmText="OK"
              confirmButtonColor="#DD6B55"
              onConfirmPressed={() => {
                this.setState({showAlert:false});
              }}
            />
        </View>
      </ImageBackground>
    );
  }

  pengaduan=()=>{
    const {data} = this.state;
    this.props.navigation.navigate('Form')
  }
}