import React, {Component} from 'react';
import {Dimensions,Picker,ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import HTML from 'react-native-render-html';

export default class detail extends Component{

  render() {
    const {params} = this.props.navigation.state;

    return (
      <View style={styles.subcontainer}>
        <View style={styles.header}>
        <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>{params.nama}</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} style={{width:Dimensions.get('window').width,marginBottom:10}}>
          <Image style={styles.ImageLogo} source={require('../image/back2.png')}/>
          <View style={{backgroundColor:'#fff',elevation:5,margin:10}}>
            <Text numberOfLines={3} style={{width:300,fontWeight:'bold',marginHorizontal:20,marginVertical:20,color:'#149BE5'}}>
                {params.judul}
            </Text>
            <View style={{marginHorizontal:10}}>
              <HTML style={{marginHorizontal:20}} html={params.deskripsi} imagesMaxWidth={Dimensions.get('window').width}/>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}