import React, {Component} from 'react';
import {ActivityIndicator,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import Communications from 'react-native-communications';
import store from 'react-native-simple-store';
const haversine = require('haversine')
import getDirections from 'react-native-google-maps-directions'
import SearchInput, { createFilter } from 'react-native-search-filter';

const KEYS = ['perizinanName'];

export default class akademik extends Component{
  constructor(props){
    super(props);
    this.state = {
      showAlert: false,
      loading:true,
      data :[],
      searchTerm: ''
      }
    }

  componentWillMount(){
      const { params } = this.props.navigation.state;
      store.get('position').then((res) =>{
        fetch('https://indramayuapp.nusantech.co/v1/subperizinan/' + params.id, {
          method: 'GET',
          headers: {  
                'Accept':'application/json',
                'Content-Type':'application/json'}
              })
        .then((response) => response.json())
        .then((responseJson) =>{
          data = responseJson
            this.setState({
              data : data,loading:false
            })
        })
      })
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }

  render() {
    const { data,loading } = this.state
    const { params } = this.props.navigation.state;
    var re = /(<[^>]+>|<[^>]>|<\/[^>]>)/g;
    const filtered = data.filter(createFilter(this.state.searchTerm, KEYS))

    if (loading === true) {
        return(
          <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
            <ActivityIndicator
              color={`rgba(0, 163, 192, 1)`}
              size={`large`}
              style={styles.s34e24b9b}
            />
            <Text style={{paddingTop:20}}> Menunggu Sebentar </Text>
          </View>
        )
      }

    return (
      <View style={styles.subcontainer}>
        <Image style={styles.ImageLogo} source={require('../image/back2.png')}/>
        <View style={styles.header}>
        <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={{position:'absolute',zIndex: 1}}>
            <Image source={require('../image/arrow.png')} style={{width:20,height:20,tintColor:'#fff',marginLeft:20,marginTop:3}}/>
          </TouchableOpacity>
          <Text style={styles.headertext}>{params.nama}</Text>
        </View>
        <View style={styles.const}>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center',marginTop:20,marginBottom:20}}>
          <View style={{backgroundColor:'#fff',marginTop:20,width:320,height:120,alignItems:'center',elevation:5,borderRadius:5,marginBottom:20,borderWidth: 0.5,borderColor: '#149BE5'}}>
            <Text style={{fontWeight:'bold',width:100,marginTop:10,marginRight:170,marginBottom:5}}>
              Search
            </Text>
            <View style={styles.textinput}>
              <SearchInput 
                onChangeText={(term) => { this.searchUpdated(term) }} 
                style={styles.searchInput}
                placeholder=" "
              />
            </View>
          </View>
        {filtered.map((data)=>
          <View style={{marginHorizontal:2}}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Detailperizinan',{nama: params.nama,deskripsi: data.perizinanDescription,judul:data.perizinanName})} style={{width:320,height:70,backgroundColor:'#fff',borderRadius:5,elevation:5,justifyContent:'center',borderWidth: 0.5,borderColor: '#149BE5'}}>
              <Text numberOfLines={2} style={{width:250,fontSize:14,color:'#149BE5',marginLeft:15}}>{data.perizinanName}</Text>
              <Image source={require('../image/rarrow.png')} style={{width:20,height:20,tintColor:'#149BE5',position:'absolute',marginLeft:280}}/>
            </TouchableOpacity>
          </View>
          )
        }
        <View style={{alignItems:'center',marginVertical:10,marginBottom:20}}>
          <TouchableOpacity style={{borderRadius:30,backgroundColor:'#149BE5',height:52,width:300,justifyContent:'center',elevation:5}} disabled={this.state.button} onPress={()=>this.props.navigation.navigate('Web')}>
            <Text style={{color:'#FFFFFF',fontSize: 15,textAlign:'center',fontWeight:'bold'}}>Ke Web DPMPTSP Indramayu</Text>
          </TouchableOpacity>
        </View>
        </ScrollView>
        </View>
      </View>
    );
  }
}