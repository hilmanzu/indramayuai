import React from 'react';
import {BackHandler,Alert,View, Text,  StyleSheet, Image ,PermissionsAndroid,Platform,ActivityIndicator} from 'react-native';
import store from 'react-native-simple-store';
import DropdownAlert from 'react-native-dropdownalert';

export default class Splashscreen extends React.Component {

    state = {
        currentLongitude: 'unknown',
        currentLatitude: 'unknown',
    }

    componentDidMount = () => {
        var that =this;
        //Checking for the permission just after component loaded
        if(Platform.OS === 'android'){
            this.callLocation(that);
        }else{
            async function requestCameraPermission() {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{
                            'title': 'Lokasi Sangat Dibutuhkan',
                            'message': 'Aplikasi ini membutuhkan lokasi anda'
                        }
                    )
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        that.callLocation(that);
                    } else {
                        Alert.alert('Aplikasi tidak dapat berjalan','Harap hidupkan lokasi anda',
                          [
                           {text: 'Oke', onPress: () => BackHandler.exitApp()},
                          ],
                         { cancelable: false })
                        return true;
                    }
                } catch (err) {
                    alert("err",err);
                    console.warn(err)
                }
            }
            requestCameraPermission();
        }    
    }

    callLocation(that){
        //alert("callLocation Called");
        navigator.geolocation.getCurrentPosition(
            //Will give you the current location
            (position) => {
                const currentLongitude = parseFloat(position.coords.longitude);
                //getting the Longitude from the location json
                const currentLatitude = parseFloat(position.coords.latitude);
                //getting the Latitude from the location json
                that.setState({ currentLongitude:currentLongitude });
                //Setting state Longitude to re re-render the Longitude Text
                that.setState({ currentLatitude:currentLatitude });
                //Setting state Latitude to re re-render the Longitude Text
            },
            (error) => {}
            ,{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
        that.watchID = navigator.geolocation.watchPosition((position) => {
            //Will give you the location on location change
            const currentLongitude = parseFloat(position.coords.longitude);
            //getting the Longitude from the location json
            const currentLatitude = parseFloat(position.coords.latitude);
            //getting the Latitude from the location json
            that.setState({ currentLongitude:currentLongitude });
            //Setting state Longitude to re re-render the Longitude Text
            that.setState({ currentLatitude:currentLatitude });
            //Setting state Latitude to re re-render the Longitude Text
            var lastRegion = {
                  latitude: currentLatitude,
                  longitude: currentLongitude,
                  longitudeDelta:0.01,
                  latitudeDelta: 0.01,
                }
            store.save('position',{initialPosition: lastRegion})

            store.get('position').then((res)=>{
              store.get('token').then((uid) =>{
                if (uid && res.initialPosition.latitude) {this.props.navigation.navigate('Menu')}
                else if (uid) {this.props.navigation.navigate('Menu')}
                else if (res.initialPosition.latitude) {this.props.navigation.navigate('Intro')}
                else {this.props.navigation.navigate('Intro')}
              })
            })

        });

    }

    componentWillUnmount = () => {
        navigator.geolocation.clearWatch(this.watchID);
    }

    render() {
    return (
      <View style={styles.container}>
        <Image source={require('./image/logo.png')} style={styles.image} />
          <ActivityIndicator
            color={`rgba(0, 163, 192, 1)`}
            size={`large`}
            style={styles.spin}
          />
        <DropdownAlert ref={ref => this.dropdown = ref} />
      </View>
        )
    }
}

const styles = StyleSheet.create ({
   container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  image: {
    width: 200,
    height: 200,
    marginBottom:20
  }
})