import React, {Component} from 'react';
import {Alert,ActivityIndicator,Platform, StyleSheet, Text, View,Image,ImageBackground,TextInput,TouchableOpacity} from 'react-native';
import styles from '../style/style'
import store from 'react-native-simple-store';
import AwesomeAlert from 'react-native-awesome-alerts';

export default class reset extends Component{
	constructor(props){
	  super(props);
	  this.state = {
	    username:'',
	    password:'',
      showAlert: false,
      button:false
	    }
	  }

  render() {
    const {showAlert} = this.state;
    return (
      <View style={styles.container}>
      	<Image style={styles.ImageLogo} source={require('../image/back2.png')}/>
      	<View style={styles.square}>
      		<Text style={styles.textphone}>
      			Email
      		</Text>
      		<View style={styles.textinput}>
            <TextInput style={{fontSize:12}} onChangeText={(username)=> this.setState({username})}/>
          </View>
      		<View style={{alignItems:'center',marginTop:20}}>
	      		<TouchableOpacity style={styles.button} disabled={this.state.button} onPress={this.login}>
	            <Text style={styles.textbutton}>RESET</Text>
	          </TouchableOpacity>
          </View>
        </View>
        <View style={styles.oval}>
          <Image style={{width:100,height:100}} source={require('../image/logo.png')}/>
      	</View>
        <AwesomeAlert
          show={showAlert}
          showProgress={true}
          message="Harap Menunggu"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
      </View>
    );
  }

  login=()=>{
    if (this.state.username == ''){
      alert('Isi Email anda')
    }else if (this.state.password == ''){
      alert('Isi Password anda')
    }else {
      this.setState({showAlert: true})
      fetch('https://indramayuapp.nusantech.co/v1/auth/signin', {
        method: 'POST',
        headers: {'Content-Type': 'application/json','Accept' : 'application/json' 
        },
        body: JSON.stringify({
            email: this.state.username,
            password: this.state.password,
        })
      })
      .then((response)=> response.json())
      .then((responseJson) =>{    
        if (responseJson.success){
          store.save('token', responseJson.token)
          Alert.alert('Selamat Datang','Aplikasi ini siap melayani anda selama berada di kabupaten Indramayu')
          this.props.navigation.navigate('Menu')
          this.setState({showAlert: false,button:false})
        }else{
        Alert.alert('Tidak Dapat Masuk',responseJson.message)
        this.setState({showAlert: false,button:false})}
      })
      .catch((error) =>{
          store.save('token', responseJson.token)
          Alert.alert('Selamat Datang','di Indramayu')
          this.setState({color: '#fff',button:false})
      })
    }
  }
}