import React, {Component} from 'react';
import {Dimensions,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../style/style'

export default class Intro extends Component{

  render() {
    const { navigate } = this.props.navigation;

    return (
      <ImageBackground style={styles.ImageBackground} source={require('../image/back.png')}>
        <View style={styles.introcontainer}>
            <Text style={styles.texthallo}>
              Halo !
            </Text>
            <Text style={styles.textindramayu}>
              Indramayu <Text style={{fontWeight:'bold'}}>All in One</Text> siap melayani anda selama berada di kabupaten Indramayu
            </Text>
            <View style={{marginTop:40}}>
              <TouchableOpacity style={styles.button} onPress={()=>navigate('Login')}>
                  <Text style={styles.textbutton}>LOGIN</Text>
              </TouchableOpacity>
            </View>
            <View style={{marginTop:20}}>
              <TouchableOpacity style={styles.button} onPress={()=>navigate('Register')}>
                  <Text style={styles.textbutton}>DAFTAR</Text>
              </TouchableOpacity>
            </View>

        </View>
      </ImageBackground>
    );
  }
}