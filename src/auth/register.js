import React, {Component} from 'react';
import {ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../style/style'
import AwesomeAlert from 'react-native-awesome-alerts';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

var radio_props = [
  {label: 'Laki-Laki', value: 'Laki-Laki' },
  {label: 'Perempuan', value: 'Perempuan' }
];

export default class login extends Component{
  constructor(props){
    super(props);
    this.state = {
      email   :'',
      password:'',
      ktp     :'',
      fullName   :'',
      last    :'',
      hp      :'',
      gender  :'',
      showAlert: false,
      color   :'rgba(247, 247, 247, 1)'
      }
    }

    ShowMaxAlert = (EnteredValue) =>{ 
      var TextLength = EnteredValue.length.toString()
      if(TextLength == 17){
        Alert.alert("Maaf, No.Ktp maksimum 16 karakter")
      }else{
        this.setState({ktp:EnteredValue})
      } 
    }

  render() {
    return (
      <View style={styles.subcontainer}>
        <View style={styles.header}>
          <Text style={styles.headertext}>Register</Text>
        </View>
        <View style={styles.const}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={styles.textfirst}>
            Nomor KTP
          </Text>
          <View style={styles.textinput}>
            <TextInput keyboardType='numeric' maxLength={16} onChangeText={EnteredValue => this.ShowMaxAlert(EnteredValue)}/>
          </View>
          <Text style={styles.textpassword}>
            Nama Lengkap
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(fullName)=> this.setState({fullName})}/>
          </View>
          <Text style={styles.textpassword}>
            Email
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(email)=> this.setState({email})}/>
          </View>
          <Text style={styles.textpassword}>
            Password
          </Text>
          <View style={styles.textinput}>
              <TextInput secureTextEntry={true} onChangeText={(password)=> this.setState({password})}/>
          </View>
          <Text style={styles.textpassword}>
            Nomor Handphone
          </Text>
          <View style={styles.textinput}>
              <TextInput keyboardType='numeric' onChangeText={(hp)=> this.setState({hp})}/>
          </View>
          <Text style={styles.textpassword}>
            Jenis Kelamin
          </Text>
          <View style={styles.textpassword}>
            <RadioForm
              radio_props={radio_props}
              initial={'Laki-Laki'}
              onPress={(value) => {this.setState({gender:value})}}
            />
          </View>
          <View style={{alignItems:'center',marginVertical:20}}>
            <TouchableOpacity style={styles.button} disabled={this.state.button} onPress={this.daftar}>
              <Text style={styles.textbutton}>Buat Akun</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        </View>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          message="Harap Menunggu"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
      </View>
    );
  }

  daftar=()=>{
    if (this.state.ktp == ''){
      alert('Isi No KTP')
    }else if (this.state.fullName == ''){
      alert('Isi Nama Lengkap')
    }else if (this.state.email == ''){
      alert('Isi Email')
    }else if (this.state.password <= 8 ){
      alert('Isi password anda')
    }else if (this.state.hp == ''){
      alert('Isi No Handphone')
    }else if (this.state.gender == ''){
      alert('Isi Jenis Kelamin')
    }else {
      this.setState({showAlert: true})
      fetch('https://indramayuapp.nusantech.co/v1/auth/signup', {
        method: 'POST',
        headers: {'Content-Type': 'application/json','Accept':'application/json' 
        },
        body: JSON.stringify({
            fullName  : this.state.fullName,
            email     : this.state.email,
            password  : this.state.password,
            phone     : this.state.hp,
            noKtp     : this.state.ktp,
            gender    : this.state.gender
        })
      })
      .then((response)=> response.json())
      .then((responseJson) =>{    
        if (responseJson.id){
          Alert.alert('Selamat Datang', 'Pendaftaran Berhasil')
          this.setState({showAlert: false})
          this.props.navigation.navigate('Login')
        }else{
          Alert.alert('Tidak Dapat Mendaftar',responseJson[0].message)
          this.setState({showAlert: false})
        }
      })
      .catch((error) =>{
          Alert.alert('Tidak dapat daftar','Periksa Koneksi Anda')
          this.setState({button:false})
      })
    }
  }

}