import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {ScrollView, View,StyleSheet,TouchableOpacity,Image} from 'react-native';
import store from 'react-native-simple-store';
import { Container,Text, Header, Content, Item,Separator,Footer, Input,Button, Icon ,Drawer,Label,Card,CardItem,Left,List, ListItem,Form, Body, Right} from 'native-base';


export default class sideMenu extends Component {
  constructor(props) {
    super(props)
      this.state = {
        dataSource: '',
        loading: true,
        color : 0
      }
  }

  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }


componentWillMount(){
      const { navigate } = this.props.navigation;
      store.get('token').then((res) =>{
        fetch('https://indramayuapp.nusantech.co/v1/profile', {
          method: 'GET',
          headers: {  
                'Accept':'application/json',
                'Content-Type':'application/json',
                'Authorization': res }
          
              })
          .then((response) => response.json())
          .then((responseJson) =>{
            if (responseJson.id){
              store.save('id', responseJson.id)
              this.setState({ dataSource: responseJson, loading: false })
            }
            else{
              this.props.navigation.navigate('Intro')
              store.delete('token')
            }
          })
      })
  }

  render () {
    const { dataSource, loading} = this.state;
    return (
      <Container style={{flex:1,backgroundColor:'#fff'}}>
          <View style={{backgroundColor:'#149BE5',paddingTop:10}}>
              <TouchableOpacity onPress={() => {this.props.navigation.closeDrawer()}} style={{backgroundColor:'#149BE5',marginLeft:245}}>
                <Image source={require('./image/cancel.png')} style={{width:20,height:20,tintColor:'#fff'}}/>
              </TouchableOpacity>
          </View>
        <View style={{justifyContent: 'center',alignItems: 'center',backgroundColor:'#149BE5'}}>
          <Image style={{resizeMode:'cover',width:40,height:40,marginHorizontal:20,marginBottom:10,tintColor:'#fff'}} source={require('./image/profile.png')}/>
          <Text style={{ textAlign: 'center',color:'#fff',marginBottom:10,fontWeight:'bold' }}>{dataSource.fullName}</Text>
        </View>
        <Content style={{flex:1}}>
          <List>
            <TouchableOpacity style={{backgroundColor:this.state.color === 0 ? '#017CC0' : '#fff',flexDirection:'row',height:50,alignItems:'center',}} onPress={this.Menu}>
                <Image source={require('./image/home.png')} style={{width:20,height:20,marginLeft:20,tintColor: this.state.color === 0 ? '#fff' : '#149BE5'}}/>
                <Text style={{color:this.state.color === 0 ? '#fff' : '#149BE5',fontWeight:'bold',marginLeft:20}}> Home </Text>
            </TouchableOpacity>

            <TouchableOpacity style={{backgroundColor: this.state.color === 1 ? '#017CC0' : '#fff',flexDirection:'row',height:50,alignItems:'center',}} onPress={this.Layanan}>
              <Image source={require('./image/layanan.png')} style={styles.icon}/>
              <Text style={{color:this.state.color === 1 ? '#fff' : '#ef5350',fontWeight:'bold',marginLeft:20}}> Layanan Darurat </Text>
            </TouchableOpacity>

              <TouchableOpacity style={{backgroundColor: this.state.color === 3 ? '#017CC0' : '#fff',flexDirection:'row',height:50,alignItems:'center',}} onPress={this.Pengaduan}>
                <Image source={require('./image/Pengaduan.png')} style={styles.icon}/>
                <Text style={{color:this.state.color === 3 ? '#fff' : '#149BE5',fontWeight:'bold',marginLeft:20}}> Pengaduan </Text>
              </TouchableOpacity>

              <TouchableOpacity style={{backgroundColor: this.state.color === 4 ? '#017CC0' : '#fff',flexDirection:'row',height:50,alignItems:'center',}} onPress={this.Kesehatan}>
                <Image source={require('./image/Hospital.png')} style={styles.icon}/>
                <Text style={{color:this.state.color === 4 ? '#fff' : '#149BE5',fontWeight:'bold',marginLeft:20}}> Kesehatan </Text>
              </TouchableOpacity>

              <TouchableOpacity style={{backgroundColor: this.state.color === 5 ? '#017CC0' : '#fff',flexDirection:'row',height:50,alignItems:'center',}} onPress={this.Ibadah}>
                <Image source={require('./image/peribadatan.png')} style={styles.icon}/>
                <Text style={{color:this.state.color === 5 ? '#fff' : '#149BE5',fontWeight:'bold',marginLeft:20}}> Peribadatan </Text>
              </TouchableOpacity>

              <TouchableOpacity style={{backgroundColor: this.state.color === 6 ? '#017CC0' : '#fff',flexDirection:'row',height:50,alignItems:'center',}} onPress={this.Pekerjaan}>
                <Image source={require('./image/Loker.png')} style={styles.icon}/>
                <Text style={{color:this.state.color === 6 ? '#fff' : '#149BE5',fontWeight:'bold',marginLeft:20}}> Lowongan Kerja </Text>
              </TouchableOpacity>

              <TouchableOpacity style={{backgroundColor: this.state.color === 7 ? '#017CC0' : '#fff',flexDirection:'row',height:50,alignItems:'center',}} onPress={this.Pariwisata}>
                <Image source={require('./image/pariwisata.png')} style={styles.icon}/>
                <Text style={{color:this.state.color === 7 ? '#fff' : '#149BE5',fontWeight:'bold',marginLeft:20}}> Pariwisata </Text>
              </TouchableOpacity>

             <TouchableOpacity style={{backgroundColor: this.state.color === 8 ? '#017CC0' : '#fff',flexDirection:'row',height:50,alignItems:'center',}} onPress={this.Pendidikan}>
                <Image source={require('./image/education.png')} style={styles.icon}/>
                <Text style={{color:this.state.color === 8 ? '#fff' : '#149BE5',fontWeight:'bold',marginLeft:20}}> Pendidikan </Text>
              </TouchableOpacity>

              <TouchableOpacity style={{backgroundColor: this.state.color === 9 ? '#017CC0' : '#fff',flexDirection:'row',height:50,alignItems:'center',}} onPress={this.Perizinan}>
                <Image source={require('./image/Perizinan.png')} style={styles.icon}/>
                <Text style={{color:this.state.color === 9 ? '#fff' : '#149BE5',fontWeight:'bold',marginLeft:20}}> Perizinan </Text>
              </TouchableOpacity>

              <TouchableOpacity style={{backgroundColor: this.state.color === 10 ? '#017CC0' : '#fff',flexDirection:'row',height:50,alignItems:'center',}} onPress={this.Transportasi}>
                <Image source={require('./image/transportasi.png')} style={styles.icon}/>
                <Text style={{color:this.state.color === 10 ? '#fff' : '#149BE5',fontWeight:'bold',marginLeft:20}}> Transportasi </Text>
              </TouchableOpacity>

              <TouchableOpacity style={{backgroundColor: this.state.color === 11 ? '#017CC0' : '#fff',flexDirection:'row',height:50,alignItems:'center',}} onPress={this.Berita}>
                <Image source={require('./image/news.png')} style={styles.icon}/>
                <Text style={{color:this.state.color === 11 ? '#fff' : '#149BE5',fontWeight:'bold',marginLeft:20}}> Berita </Text>
              </TouchableOpacity>

              <TouchableOpacity style={{backgroundColor:'#fff',flexDirection:'row',height:50,alignItems:'center',}} onPress={this.logout}>
                <Image source={require('./image/cancel.png')} style={styles.icon}/>
                <Text style={{color:'#149BE5',fontWeight:'bold',marginLeft:20}}> Logout </Text>
              </TouchableOpacity>
          </List>
        </Content>
      </Container>
    );
  }

  Menu=()=>{
    this.props.navigation.navigate('Menu')
    this.setState({color:0})
  }

  Layanan=()=>{
    this.props.navigation.navigate('Layanan')
    this.setState({color:1})
  }

   kk=()=>{
    this.props.navigation.navigate('Kuning')
    this.setState({color:2})
  }

  Pengaduan=()=>{
    this.props.navigation.navigate('Pengaduan')
    this.setState({color:3})
  }

  Kesehatan=()=>{
    this.props.navigation.navigate('Jeniskesehatan')
    this.setState({color:4})
  }

  Ibadah=()=>{
    this.props.navigation.navigate('Jenisibadah')
    this.setState({color:5})
  }

  Pekerjaan=()=>{
    this.props.navigation.navigate('JenisPekerjaan')
    this.setState({color:6})
  }

  Pariwisata=()=>{
    this.props.navigation.navigate('Pariwisata')
    this.setState({color:7})
  }

  Pendidikan=()=>{
    this.props.navigation.navigate('Jenispendidikan')
    this.setState({color:8})
  }

  Perizinan=()=>{
    this.props.navigation.navigate('Perizinan')
    this.setState({color:9})
  }

  Transportasi=()=>{
    this.props.navigation.navigate('Transportasi')
    this.setState({color:10})
  }

  Berita=()=>{
    this.props.navigation.navigate('Berita')
    this.setState({color:11})
  }

  logout=()=>{
    this.props.navigation.navigate('Intro')
    store.delete('token')
  }

}

sideMenu.propTypes = {
  navigation: PropTypes.object
};

const styles = StyleSheet.create({
    icon: {width:20,height:20,marginLeft:20},
    icon2: {width:20,height:20,tintColor:'#fff'},
    color: {backgroundColor:'#fff'},
    text:{color:'#ef5350',fontWeight:'bold',marginLeft:20},
    text2:{color:'#149BE5',fontWeight:'bold',marginLeft:20},
    text3:{color:'#fff',fontWeight:'bold',marginLeft:20},
})
